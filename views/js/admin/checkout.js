/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (typeof RESURSBANK === 'undefined') {
    var RESURSBANK = {};
}

RESURSBANK.CheckoutAdmin = (function (me) {

    /**
     * Observe production mode radio button to remind client to re-register callbacks and fetch payment methods when
     * changing environments.
     */
    me.observeProductionMode = function () {
        $('input[name="RESURSBANKCHECKOUT_PRODUCTION_MODE"]').change(function (val) {
            alert('När du byter mellan produktion- och testmiljön måste du även registrera om dina callbacks och hämta betalmetoder på nytt. Spara konfigurationen för att byta miljö och låt sidan ladda om. Klicka därefter på knapparna "Hämta tillgängliga betalmetoder" och "Registrera callback URL:er" för att hämta betalmetoder och registrera om callbacks.');
        });
    };

    /**
     * Register callback URLs.
     */
    me.registerCallbacks = function () {
        // 1. Display a new status indicator, letting the client know we are updating all callbacks.
        $('#resursCallbackListStatus').html('<b>Please wait while updating callbacks for environment: ' + RESURSBANK_CURRENT_ENVIRONMENT + '...</b>').css({color: '#990000'});
        $('#resursCallbackListStatus').show();

        // 2. Clear current URL values, replacing them with a placeholder.
        $("[id^=CB_URL_]").each(function (i, e) {
            e.innerHTML = "Updating...";
        });

        // 3. Perform the actual AJAX call to register callbacks.
        $.ajax({
            type: 'GET',
            url: RESURSBANK_ADMIN_CONTROLLER_URL,
            data: {
                ajax: true,
                action: 'registercallbacks'
            },
            dataType: 'json',
            success: function (response) {
                // Handle response "data".
                var data = response.hasOwnProperty('data') ? response.data : null;

                if (
                    data !== null &&
                    data.hasOwnProperty('callbacks')
                ) {
                    $('#resursbank-callback-list').html(me.generateCallbackListContent(data));
                }

                if (response.message.error.length) {
                    alert(response.message.error);
                } else {
                    $('#resursCallbackListStatus').html('<b>Successfully registered callbacks for environment: ' + RESURSBANK_CURRENT_ENVIRONMENT + '</b>').css({'color': '#72c279'});
                }
            },
            failure: function (response) {
                alert('Something went wrong while attempting to register your callbacks.');
            }
        })
    };

    /**
     * Generate update callback list content.
     *
     * @param {Object} data
     * @returns {string}
     */
    me.generateCallbackListContent = function (data) {
        var result = '';

        if (
            data !== null &&
            data.hasOwnProperty('callbacks')
        ) {
            for (var key in data.callbacks) {
                result += [
                    '<div class="row">' +
                    '<div class="col-lg-3">' +
                    key +
                    '</div>' +
                    '<div style="overflow-wrap: break-word;" id="CB_URL_' + key + '">' +
                    data['callbacks'][key] +
                    '</div>' +
                    '</div>'
                ].join("\n");
            }
        }

        return result;
    };

    /**
     * Meta Lang "/För"-fix.
     */
    me.fixTheMetaLangError = function() {
        if (confirm('This action will try to fix the meta_lang issue by replacing "För" with "order" - the method itself is not supported by Resurs Bank and you are running it on your own risk. Do you want to proceed?')) {
            $.ajax({
                type: 'GET',
                url: RESURSBANK_ADMIN_CONTROLLER_URL,
                data: {
                    ajax: true,
                    action: 'fixTheMetaLang'
                },
                dataType: 'json',
                success: function (response) {
                    if (typeof response.data.reload !== 'undefined') {
                        document.location = document.location.href;
                    }
                    if (response.message.error.length) {
                        alert(response.message.error);
                    }
                },
                failure: function (response) {
                    alert('Something went wrong while fetching available payment methods.');
                }
            })
        }
    };

    /**
     * Fetch payment methods.
     */
    me.fetchPaymentMethods = function () {
        $('#resursbank-checkout-fetch-payment-methods-status').html('<b>Please wait while fetching payment methods...</b>');
        $('#resursbank-checkout-fetch-payment-methods-status').show();

        $.ajax({
            type: 'GET',
            url: RESURSBANK_ADMIN_CONTROLLER_URL,
            data: {
                ajax: true,
                action: 'fetchPaymentMethods'
            },
            dataType: 'json',
            success: function (response) {
                if (response.message.error.length) {
                    $('#resursbank-checkout-fetch-payment-methods-status').html(
                        '<b>'+response.message.error+'</b>'
                    );
                    //alert(response.message.error);
                } else {
                    if (typeof response.data.method !== 'undefined') {
                        $('#resursbank-checkout-fetch-payment-methods-status').html(
                            '<b>Done! Page will now reload. Please wait...</b>'
                        );
                        document.location = document.location.href;
                    }
                }
            },
            failure: function (response) {
                alert('Something went wrong while fetching available payment methods.');
            }
        })
    };

    me.updateAnnuityDuration = function (annuityObject) {
        $('#resursbank-checkout-fetch-payment-methods-status').html('<b>Please wait while saving annuity factor duration...</b>');
        $('#resursbank-checkout-fetch-payment-methods-status').show();
        $.ajax({
            type: 'GET',
            url: RESURSBANK_ADMIN_CONTROLLER_URL,
            data: {
                ajax: true,
                action: 'updateAnnuityDuration',
                data: annuityObject.value
            },
            dataType: 'json',
            success: function (response) {
                if (response.message.error.length) {
                    alert(response.message.error);
                }
                $('#resursbank-checkout-fetch-payment-methods-status')
                    .html('<b>Annuity factor duration successfully saved</b>')
                    .delay(2000)
                    .fadeOut(600);
            },
            failure: function (response) {
                alert('Something went wrong while fetching available payment methods.');
            }
        })
    };

    return me;
}(Classy.create(null)));
