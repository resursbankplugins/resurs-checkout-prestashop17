/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (typeof RESURSBANK === 'undefined') {
    var RESURSBANK = {};
}

RESURSBANK.Checkout = (function (me) {
    /**
     * Whether or not [Checkout] has been initialized.
     *
     * @type {Boolean}
     */
    var initialized = false;

    /**
     * Whether or not [finalizeIframeSetup()] has been called.
     *
     * @type {boolean}
     */
    var finalizedIframe = false;

    /**
     * The iframe element.
     *
     * @type {Element}
     */
    var iframe = null;

    /**
     * The URL to the iframe.
     *
     * @type {String}
     */
    var iframeUrl = '';

    /**
     * The base URL.
     *
     * @type {String}
     */
    var baseUrl = '';

    /**
     * Failure URL.
     *
     * @type {String}
     */
    var failureUrl = '';

    /**
     * The URL to the saveorder controller.
     *
     * @type {String}
     */
    var saveOrderUrl = '';

    /**
     * The URL to the saveshipping controller (saves entered shipping address).
     *
     * @type {String}
     */
    var saveShippingUrl = '';

    /**
     * The URL to the savebilling controller (saves entered billing address).
     *
     * @type {String}
     */
    var saveBillingUrl = '';

    /**
     * The URL to the savepayment controller.
     *
     * @type {String}
     */
    var savePaymentUrl = '';

    /**
     * The URL to the savedelivery controller.
     *
     * @type {String}
     */
    var saveDeliveryUrl = '';

    /**
     * The URL to the internal cart controller.
     *
     * @type {String}
     */
    var cartControlUrl = '';

    /**
     * Whether or not to store SSN on billing address.
     *
     * @type {Boolean}
     */
    var storeSsn = false;

    /**
     * Handles user address changes.
     *
     * @type {UserAddress}
     */
    var userAddress = null;

    /**
     * A screen overlay for loading purposes.
     *
     * @type {AjaxOverlay}
     */
    var ajaxOverlay = null;

    /**
     * Handles payment method changes.
     *
     * @type {PaymentMethod}
     */
    var paymentMethod = null;

    /**
     * Handles the order booking.
     *
     * @type {BookOrder}
     */
    var bookOrder = null;

    /**
     * Handles the shipping methods on the checkout page.
     *
     * @type {DeliveryMethod}
     */
    var deliveryMethod = null;

    /**
     * An ErrorHandler.
     *
     * @type {ErrorHandler}
     */
    var errorHandler = null;

    /**
     * Receives and delegates messages from the Resursbank_Checkout iframe when it uses window.postMessage().
     *
     * @param {Object} event
     */
    var postMessageDelegator = function (event) {
        var data;
        var origin = event.origin || event.originalEvent.origin;

        if (origin !== iframeUrl ||
            typeof event.data !== 'string' ||
            event.data === '[iFrameResizerChild]Ready') {
            return;
        }

        var jsonTest = {};

        try {
            jsonTest = JSON.parse(event.data);
        } catch (e) {
        }

        data = jsonTest;
        if (data.hasOwnProperty('eventType') && typeof data.eventType === 'string') {
            switch (data.eventType) {
                case 'checkout:user-info-change':
                    userAddressChange(data);
                    break;
                case 'checkout:payment-method-change':
                    paymentMethodChange(data);
                    break;
                case 'checkout:puchase-button-clicked':
                    onBookOrder(data);
                    break;
                case 'checkout:loaded':
                    finalizeIframeSetup();
                    break;
                case 'checkout:purchase-failed':
                    purchaseFailed();
                    break;
                case 'checkout:purchase-denied':
                    purchaseFailed();
                    break;
                default:
                    ;
            }
        }
    };

    /**
     * Posts a message to the parent window with postMessage(). The data argument will be sent to the parent window and
     * have an eventType property which is set when this function is called.
     *
     * @param data {Object} Information to be passed to the iframe.
     */
    var postMessage = function (data) {
        var iframeWindow;

        if (iframe instanceof HTMLIFrameElement &&
            typeof iframeUrl === 'string' &&
            iframeUrl !== ''
        ) {
            iframeWindow = iframe.contentWindow || iframe.contentDocument;
            iframeWindow.postMessage(JSON.stringify(data), iframeUrl);
        }
    };

    /**
     * When the iframe is done loading, this method will do some final work in order to make the checkout page and
     * the iframe communicate and work together as intended.
     *
     * NOTE: Should only be called once when the iframe has fully loaded. That is, when it has done all of its AJAX
     * requests and other preparations.
     */
    var finalizeIframeSetup = function () {
        if (!finalizedIframe) {
            // Post the booking rule to Resursbank_Checkout. This post basically says that when a user presses the button
            // to finalize the order, check with the server if the order is ready to be booked.
            postMessage({
                eventType: 'checkout:set-purchase-button-interceptor',
                checkOrderBeforeBooking: true
            });

            finalizedIframe = true;
        }
    };

    /**
     *  Takes an HTML-string that is going to replace the HTML of the cart
     *  summary block.
     *
     * @param {string} html
     */
    var updateCartSummary = function (html) {
        $('#js-checkout-summary').replaceWith(html);
        $('.cart-summary-products').hide();
    };

    /**
     * When the purchase fails (for example because signing doesn't work) we redirect to the order failure page.
     */
    var purchaseFailed = function () {
        window.location = failureUrl;
    };

    /**
     * Takes an object with the new address information and pushes it to the server.
     *
     * @param {Object} data - The new address information.
     */
    var userAddressChange = function (data) {
        me.AjaxQ.getChain('resursbank-checkout').onDone(function () {
        }, true);

        userAddress.push(data);
    };

    /**
     * Takes an object with the new payment method information and pushes it to the server.
     *
     * @param {Object} data - The payment method information.
     */
    var paymentMethodChange = function (data) {
        me.AjaxQ.getChain('resursbank-checkout').onDone(function () {}, true);

        paymentMethod.$call('push', paymentMethod.$call('prepareData', data));
    };

    /**
     * Callback for when the iframe's purchase button has been clicked.
     *
     * @param {Object} data - Data from the iframe.
     */
    var onBookOrder = function (data) {
        me.AjaxQ.getChain('resursbank-checkout').onDone(function () {
        }, true);

        bookOrder.$call('push');
    };

    /**
     * Error handling when order cannot be placed.
     *
     * @param {Object} data
     * @param {String} iframeMessage
     */
    var onBookOrderPushError = function (data, iframeMessage) {
        postMessage(iframeMessage);

        // errorHandler.feedLine('The page will reload. An error occured when booking your order:')
        //     .feedLines(data.message.error)
        //     .alert();

        location.reload();
    };

    /**
     * Event handler for when an error occurs when pushing the payment method.
     *
     * @param {Object} data
     */
    var onPaymentMethodPushError = function (data) {
        errorHandler.feedLine('An error occured when processing the chosen payment method:')
            .feedLines(data.message.error)
            .alert();
    };

    /**
     * Event handler for when the user address ahs been pushed to the server.
     *
     * @param {Object} data
     */
    var onUserAddressError = function (data) {
        // errorHandler.feedLine('An error occurred while updating your address:')
        //     .feedLines(data.message.error)
        //     .alert();
    };

    /**
     * Event handler for when the user address has been pushed to the server.
     *
     * @param {Object} data
     */
    var onUserAddressPushed = function (data) {
    };


    var onDeliveryOptionUpdateSuccess = function (data) {
        if (data.hasOwnProperty('cart_summary') &&
            typeof data.cart_summary === 'string'
        ) {
            updateCartSummary(data.cart_summary);
        }
    };

    /**
     * Takes the iframe element as a string, parses it, and returns it as an
     * array where the first element is a HTMLIframeElement and the second is
     * a HTMLScriptElement.
     *
     * @param {string} iframeStr
     * @returns {Array<HTMLIFrameElement|HTMLScriptElement>}
     */
    var getIframe = function (iframeStr) {
        var parsed = $.parseHTML(iframeStr, null, true);
        var els;

        if (Array.isArray(parsed)
            && parsed.length === 1
            && typeof parsed[0].data === 'string'
        ) {
            els = $.parseHTML(parsed[0].data, null, true);
        }

        return els;
    };

    /**
     * An [AjaxQ] instance.
     *
     * @type {AjaxQ}
     */
    me.AjaxQ = null;

    /**
     * An initializer.
     *
     * @param {Object} config
     * @param {String} config.baseUrl
     * @param {String} config.failureUrl
     * @param {String} config.iframeUrl
     * @param {String} config.iframe
     * @param {String} config.saveOrderUrl
     * @param {String} config.saveBillingUrl
     * @param {String} config.saveShippingUrl
     * @param {String} config.savePaymentUrl
     * @param {String} config.saveDeliveryUrl
     * @param {String} config.cartControlUrl
     * @param {String} config.storeSsn
     * @param {String} config.formKey
     * @param {Object} config.displayedTax - Which prices that are displayed.
     * @param {Number} config.displayedTax.excl - Price with only excluding tax. Should be 1.
     * @param {Number} config.displayedTax.incl - Price with only including tax. Should be 2.
     * @param {Number} config.displayedTax.both - Price with both including and excluding tax. Should be 3.
     * @param {Number} config.displayedTax.type - Which of "both", "excl" or "incl" that is displayed. Should be one of
     * their respective numbers.
     * @return {me}
     */
    me.init = function (config) {
        var iframeEls;

        if (!initialized) {
            // ================ Initialize properties. ================
            baseUrl = config.baseUrl;
            failureUrl = config.failureUrl;
            iframeUrl = config.iframeUrl;
            saveOrderUrl = config.saveOrderUrl;
            saveBillingUrl = config.saveBillingUrl;
            saveShippingUrl = config.saveShippingUrl;
            savePaymentUrl = config.savePaymentUrl;
            saveDeliveryUrl = config.saveDeliveryUrl;
            cartControlUrl = config.cartControlUrl;
            storeSsn = Boolean(config.storeSsn);
            iframeEls = getIframe(config.iframe);

            if (typeof prestashop !== 'undefined') {
                prestashop.on('updateCart', function (event) {
                    $('.cart-summary-products').hide();
                    $.ajax({
                        url: me.getCartControlUrl() + "&action=checkcart",
                        method: 'post'
                    }).success(
                        function (response) {
                            if (typeof response.data !== 'undefined' && typeof response.data.hasProducts !== 'undefined') {
                                if (!response.data.hasProducts && response.data.go !== '') {
                                    document.location = response.data.go;
                                }
                            }
                        }
                    );
                });
            }

            if (Array.isArray(iframeEls) &&
                iframeEls[0] instanceof HTMLIFrameElement &&
                iframeEls[1] instanceof HTMLScriptElement
            ) {
                iframe = iframeEls[0];
                $('#resursbank-checkout-iframe-section')
                    .append(iframeEls[0])
                    .append(iframeEls[1]);
            }

            me.AjaxQ = Classy.create('AjaxQ');
            me.AjaxQ.createChain('resursbank-checkout');

            bookOrder = Classy.create('BookOrder');
            bookOrder.$on('pushed', postMessage);
            bookOrder.$on('push-failed', postMessage);
            bookOrder.$on('push-error', onBookOrderPushError);

            userAddress = Classy.create('UserAddress');
            userAddress.$on('pushed', onUserAddressPushed);
            userAddress.$on('error', onUserAddressError);

            paymentMethod = Classy.create('PaymentMethod');
            paymentMethod.$on('push-error', onPaymentMethodPushError);

            deliveryMethod = Classy.create('DeliveryMethod');
            deliveryMethod.$on(
                'delivery-option-update-success',
                onDeliveryOptionUpdateSuccess
            );

            errorHandler = Classy.create('ErrorHandler');

            // ================ End of initialization. ================
            // Add message listener.
            window.addEventListener('message', postMessageDelegator, false);

            initialized = true;
        }

        return me;
    };

    /**
     * Returns the base URL.
     *
     * @returns {String}
     */
    me.getBaseUrl = function () {
        return baseUrl;
    };

    /**
     * Returns URL to the saveorder controller.
     *
     * @returns {String}
     */
    me.getSaveOrderUrl = function () {
        return saveOrderUrl;
    };

    /**
     * Returns URL to the savebilling controller.
     *
     * @returns {String}
     */
    me.getSaveBillingUrl = function () {
        return saveBillingUrl;
    };

    /**
     * Returns URL to the savebilling controller.
     *
     * @returns {String}
     */
    me.getCartControlUrl = function () {
        return cartControlUrl;
    };

    /**
     * Returns URL to the saveshipping controller.
     *
     * @returns {String}
     */
    me.getSaveShippingUrl = function () {
        return saveShippingUrl;
    };

    /**
     * Returns URL to the savepayment controller.
     *
     * @returns {String}
     */
    me.getSavePaymentUrl = function () {
        return savePaymentUrl;
    };

    /**
     * Returns URL to the savedelivery controller.
     *
     * @returns {String}
     */
    me.getSaveDeliveryUrl = function () {
        return saveDeliveryUrl;
    };

    /**
     * Returns whether or not to store SSN on billing address.
     *
     * @returns {Boolean}
     */
    me.getStoreSsn = function () {
        return storeSsn;
    };

    return me;
}(Classy.create(null)));
