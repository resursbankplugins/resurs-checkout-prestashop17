/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'DeliveryMethod',

    init: function DeliveryMethod(me) {
        /**
         * The delivery option input elements.
         *
         * @type {Array}
         */
        var elements = [];

        /**
         * The chosen delivery method.
         *
         * @type {string}
         */
        var selectedId = '';

        /**
         * Checks with the server if the order is ready to be booked.
         *
         * @return {DeliveryMethod}
         */
        me.push = function () {
            RESURSBANK.Checkout.AjaxQ.queue({
                method: 'post',
                chain: 'resursbank-checkout',
                url: RESURSBANK.Checkout.$call('getSaveDeliveryUrl'),
                data: {
                    delivery_id: selectedId,
                    ajax: true
                },

                success: function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        Array.isArray(response.message.error) &&
                        response.message.error.length === 0
                    ) {
                        if (response.hasOwnProperty('data')) {
                            me.$emit(
                                'delivery-option-update-success',
                                response.data
                            );
                        }
                    }
                },

                failure: function (data) {

                }
            }).run('resursbank-checkout');

            return me;
        };

        /**
         * Sets the delivery method.
         *
         * @param {string} value
         * @returns {DeliveryMethod}
         */
        me.setSelectedId = function (value) {
            if (typeof value === 'string') {
                selectedId = value;
            }

            return me;
        };

        /**
         * Returns the delivery method.
         *
         * @returns {string}
         */
        me.getSelectedId = function () {
            return selectedId;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            me.$nullify();
        };

        function init() {
            elements = $('input[name="rbc-carrier"]').get();
            $(elements).on('change', function () {
                me.setSelectedId(this.value).push();
            });
        }

        init();
    }
});
