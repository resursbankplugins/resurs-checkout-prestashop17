<!--
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->

{capture name=path}
    <span class="navigation-pipe">{}</span>{l s='Order confirmation' mod='resursbankcheckout'}
{/capture}

{extends file='page.tpl'}

{block name='page_content'}

 <div class="row">
     <div id="center_column" class="center_column col-xs-12 col-sm-12">

         <h1 class="page-heading">{l s='Order confirmation' mod='resursbankcheckout'}</h1>

         <h3>{l s='Your order on' mod='resursbankcheckout'} {$shop_name} {l s='is complete.' mod='resursbankcheckout'}</h3>
         <p>
             <br>- {l s='Amount' mod='resursbankcheckout'} :
             <span class="price">
                {$priceWithTax}
             </span>
             <br>- {l s='Reference' mod='resursbankcheckout'} :
             <span class="reference">
                 <strong>{$order->reference}</strong>
             </span>
             <br>
             <br>{l s='An email has been sent with this information.' mod='resursbankcheckout'}
             <br>
             <br>{l s='If you have questions or concerns, please contact our' mod='resursbankcheckout'}
             <a href="{$base_url}index.php?controller=contact">{l s='customer service.' mod='resursbankcheckout'}</a>
         </p>
         <hr>
         <p class="cart_navigation exclusive">
             <a class="button-exclusive btn btn-default" href="{$base_url}/index.php?controller=guest-tracking" title="{l s='Go to your order history page' mod='resursbankcheckout'}">
                 <i class="icon-chevron-left"></i>{l s='View your order history' mod='resursbankcheckout'}</a>
         </p>
     </div>
     <!-- #center_column -->
 </div>
{/block}
