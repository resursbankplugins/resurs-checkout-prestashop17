{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{*{extends file="checkout/checkout.tpl"}*}

{*{block name='content'}*}
    {*{block name='cart_summary'}*}
        {*{render file='checkout/checkout-process.tpl' ui=$checkout_process}*}
    {*{/block}*}
{*{/block}*}

<!doctype html>
<html lang="{$language.iso_code}">
    <head>
        {block name='head'}
            {include file='_partials/head.tpl'}
        {/block}
        <style>
            .rbc-carriers {
                display: block;
            }

            .rbc-carriers > .rbc-list {
                display: flex;
                flex-direction: column;
            }

            .rbc-carriers > .rbc-list > .rbc-row {
                display: flex;
                flex-direction: row;
                flex: 1 1 100%;
                /*border: 2px solid red;*/
                text-align: left;
                padding: 10px;
            }

            .rbc-carriers > .rbc-list > .rbc-row > .rbc-info {
                flex: 1;
                padding: 10px;
                word-wrap: normal;
            }

            .rbc-carriers > .rbc-list > .rbc-row > .rbc-info.rbc-radio {
                flex: 0 0 auto;
                /*background-color: red;*/
            }

            .rbc-iframe {
                padding: 20px;
            }

            .rbc-card {
                background-color: white;
                box-shadow: 2px 2px 8px 0 rgba(0,0,0,.2);
            }
        </style>
    </head>

    <body id="{$page.page_name}" class="{$page.body_classes|classnames}">

        {block name='hook_after_body_opening_tag'}
            {hook h='displayAfterBodyOpeningTag'}
        {/block}

        <header id="header">
            {block name='header'}
                {include file='_partials/header.tpl'}
            {/block}
        </header>

        {block name='notifications'}
            {include file='_partials/notifications.tpl'}
        {/block}

        <section id="wrapper">
            {hook h="displayWrapperTop"}
            <div class="container">

                {block name='content'}
                    <section id="content">
                        <div class="row">
                            <div class="col-md-8">
                                <section class="card rbc-card">
                                    {include file='checkout/_partials/cart-detailed.tpl' cart=$cart}
                                </section>

                                <section class="rbc-carriers card rbc-card">
                                    <div class="rbc-list card-block">
                                        {foreach from=$delivery_options item=carrier key=id}
                                            <label class="rbc-row">
                                                <div class="rbc-info rbc-radio">
                                                    <input type="radio"
                                                           name="rbc-carrier"
                                                           value="{$id}"
                                                           data-rbc-carrier
                                                            {if $selected_delivery_option === $id}
                                                                checked
                                                            {/if}>
                                                </div>
                                                <div class="rbc-info">
                                                    {$carrier.name}
                                                </div>
                                                <div class="rbc-info">
                                                    {if $carrier.logo}
                                                        <img src="{$carrier.logo}" alt="{$carrier.name}" />
                                                    {/if}
                                                </div>
                                                <div class="rbc-info">
                                                    {$carrier.delay}
                                                </div>
                                                <div class="rbc-info">
                                                    {$carrier.price}
                                                </div>
                                            </label>
                                        {/foreach}
                                    </div>
                                </section>
                                {hook h="displayFooter" mod="resursbankcheckout"}
                            </div>

                            <div class="col-md-4">
                                {block name='hook_shopping_cart'}
                                    {hook h='displayShoppingCart'}
                                {/block}

                                {block name='cart_totals'}
                                    {include file='module:resursbankcheckout/views/templates/front/_partials/cart-detailed.tpl' cart=$cart}
                                {/block}

                                {hook h='displayReassurance'}
                            </div>
                        </div>
                    </section>
                {/block}
            </div>
            {hook h="displayWrapperBottom"}
        </section>

        {block name='javascript_bottom'}
            {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
        {/block}

        {block name='hook_before_body_closing_tag'}
            {hook h='displayBeforeBodyClosingTag'}
        {/block}

    </body>

</html>
