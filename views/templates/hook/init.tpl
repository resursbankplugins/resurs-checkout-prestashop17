<!--
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->

<div id="resursbank-checkout-container" style="display:none;">
    {if count($errorMessages)}
        {foreach $errorMessages as $msg}
            <div class="alert alert-danger">{$msg}</div>
        {/foreach}
    {/if}
    <section class="card rbc-iframe rbc-card" id="resursbank-checkout-iframe-section">
    </section>
</div>

<script type="text/javascript">
    window.addEventListener('load', function () {
        Classy.setWarningLevel('warn');

        // Display iframe.
        $('#resursbank-checkout-container').show();

        // Initialize checkout.
        RESURSBANK.Checkout.init({
            baseUrl: '{$baseUrl nofilter}',
            failureUrl: '{$backUrl nofilter}',
            iframeUrl: '{$iframeUrl nofilter}',
            saveOrderUrl: '{$saveOrderUrl nofilter}',
            saveBillingUrl: '{$saveBillingUrl nofilter}',
            saveShippingUrl: '{$saveShippingUrl nofilter}',
            savePaymentUrl: '{$savePaymentUrl nofilter}',
            saveDeliveryUrl: '{$saveDeliveryUrl nofilter}',
            cartControlUrl: '{$cartControlUrl nofilter}',
            iframe: '{$iframe}',
            storeSsn: {if $storeSsn === true} true {else} false {/if}
        });

    }, false);
</script>
