<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Shell;

// Requested by controllers in PrestaCore.
if (!isset($_SERVER['REQUEST_METHOD'])) {
    $_SERVER['REQUEST_METHOD'] = 'GET';
}

use Configuration;
use Exception;
use Order as PrestaOrder;
use PrestaShop\PrestaShop\Core\Exception\ContainerNotFoundException;
use Resursbank\RBEcomPHP\ResursBank;
use ResursBankCheckout;
use ResursBankCheckout\Models\Log;
use ResursBankCheckout\Models\Service\Order;
use ResursBankCheckout\Models\Service\Order\State;
use SoapFault;
use stdClass;
use TorneLIB\Module\Config\WrapperConfig;
use TorneLIB\Module\Network\NetWrapper;

require(dirname(__FILE__) . '/../../../config/config.inc.php');
//require(dirname(__FILE__) . '/../../../init.php');
require(dirname(__FILE__) . '/../resursbankcheckout.php');

/**
 * Class Cleaner
 *
 * @package ResursBankCheckout\Shell
 */
class Cleaner
{
    /**
     * Log handler instance.
     *
     * @var Log
     */
    private $logHandler;

    /**
     * Instance of Resurs Bank Checkout module.
     *
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Run cleaning job.
     */
    public function execute()
    {
        try {
            // Loop through all orders currently marked as pending.
            foreach ($this->getPendingOrders() as $id) {
                $id = (int)$id;

                $this->getLogHandler()->log('Processing order ' . $id);

                /** @var PrestaOrder $order */
                $order = $this->getOrder($id);

                // If the order is more than an hour old, and has no active
                // payment attached to it, cancel it.
                if ($this->canCancel($order)) {
                    try {
                        Order::cancel($order);
                        $this->getLogHandler()->log('Cancelled order ' . $id . ' by scheduled job');
                    } catch (Exception $e) {
                        // We're supposed to catch ContainerNotFoundException here.
                        $this->getLogHandler()->log($e);
                    }
                } else {
                    $this->getLogHandler()->log('Skipped order ' . $id);
                }
            }
        } catch (Exception $e) {
            $this->getLogHandler()->log($e);
        }
    }

    /**
     * Retrieve order from the database using its id.
     *
     * @param int $id
     * @return PrestaOrder
     * @throws Exception
     */
    private function getOrder($id)
    {
        $id = (int)$id;

        $result = new PrestaOrder($id);

        if (!($result instanceof PrestaOrder)) {
            throw new Exception('No order found with id ' . $id);
        }

        return $result;
    }

    /**
     * Retrieve array containing the id of all currently pending orders.
     *
     * @return array
     */
    private function getPendingOrders()
    {
        $result = PrestaOrder::getOrderIdsByStatus(
            (int)Configuration::get(State::PENDING)
        );

        return is_array($result) ? $result : [];
    }

    /**
     * Retrieve log handler.
     *
     * @return Log
     */
    private function getLogHandler()
    {
        if ($this->logHandler === null) {
            $this->logHandler = new Log('cron');
        }

        return $this->logHandler;
    }

    /**
     * Check whether or not we can safely cancel an order.
     *
     * @param PrestaOrder $order
     * @return bool
     * @throws Exception
     */
    private function canCancel(PrestaOrder $order)
    {
        $result = false;

        $orderIntervalExpireReal = (int)Configuration::get(
            ResursBankCheckout\Models\Service\Config::SETTING_CLEANER_INTERVAL
        );
        if (!$orderIntervalExpireReal) {
            $orderIntervalExpire = 3600;
        } else {
            $orderIntervalExpire = (int)$orderIntervalExpireReal * 60;
        }

        $uAgent = [
            sprintf('%s-%s-cronjob-cleaner', $this->getModule()->getName(), $this->getModule()->getVersion()),
            sprintf('EComPHP-%s', (new ResursBank())->getVersionNumber()),
            sprintf('NETCURL-%s', (new NetWrapper())->getVersion()),
            sprintf('PrestaShop-%s', _PS_VERSION_),
            sprintf('PHP-%s', PHP_VERSION),
        ];
        WrapperConfig::setSignature(implode('/', $uAgent));

        // First, the order must have existed for more than hour.
        $orderAmount = (float)$order->total_paid_tax_incl;
        // First, the order must have existed for more than hour - and not be a zero amount order.
        if (strtotime($order->date_add) < (time() - $orderIntervalExpire) && $orderAmount > 0) {
            // Secondly, there mustn't be any active payment attached to it. If
            // The payment cannot be fetched we will receive an Exception. We
            // also perform a safety check to future proof the solution.
            try {
                /** @var stdClass $payment */
                $payment = $this->getPayment($order);

                if (!isset($payment->id) ||
                    (string)$payment->id !== (string)$order->reference
                ) {
                    $result = true;
                }
            } catch (Exception $e) {
                $this->getLogHandler()->log($e);
                $errorCode = (int)$e->getCode();
                // If errors is related to "not found", orders should cancel regardless of remaining errors.
                // Error code 3 = Rest based payment not found error.
                // Error code 8 = SOAP based payment not found error.
                // Error code 404 = Another rest based payment not found error.
                if (in_array($errorCode, [3, 8, 404], false)) {
                    $result = true;
                    $this->getLogHandler()->log(
                        sprintf(
                            'Exception code retrieved as %d (payment not found). We can cancel this order. ExpireInterval is %d.',
                            $errorCode,
                            $orderIntervalExpireReal
                        )
                    );
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve payment from Resurs Bank API.
     *
     * @param PrestaOrder $order
     * @return stdClass
     * @throws Exception
     */
    private function getPayment(PrestaOrder $order)
    {
        if (!isset($order->reference) || (string)$order->reference === '') {
            throw new Exception('Missing order reference.');
        }

        try {
            $result = $this->getModule()
                ->getApi()
                ->ecom()
                ->getPayment((string)$order->reference);
        } catch (SoapFault $e) {
            throw $e;
        } catch (Exception $e) {
            $errorCode = $e->getCode();
            // If there is no payment we will receive an Exception from ECom.
            if (!in_array($errorCode, [3, 8, 404], true)) {
                throw $e;
            }
        }

        return isset($result) && ($result instanceof stdClass) ? $result : new stdClass();
    }

    /**
     * Retrieve module instance (in order to use the API connection).
     *
     * @return ResursBankCheckout
     */
    private function getModule()
    {
        if ($this->module === null) {
            $this->module = new ResursBankCheckout();
        }

        return $this->module;
    }
}

// Run cleaning job.
$cleaner = new Cleaner();
$cleaner->execute();
