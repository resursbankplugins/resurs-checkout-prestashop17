<?php
/**
 * Copyright 2019 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutGetCostOfPurcaseModuleFrontController
 *
 * Display cost of purchase example.
 */
class ResursBankCheckoutCostOfPurchaseModuleFrontController extends ModuleFrontController
{
    /**
     * @var \Resursbank\RBEcomPHP\ResursBank
     */
    protected $ecom;

    /**
     * ResursBankCheckoutCostOfPurchaseModuleFrontController constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->ecom = $this->getModule()->getApi()->ecom();
    }

    /**
     * Fetch template that should be injected into the annuityHtml.
     *
     * @return string
     */
    private function getAnnuityHtmlBefore()
    {
        return $this->module->display(
            $this->module->getPath('resursbankcheckout.php'),
            'annuity_html_before.tpl'
        );
    }

    /**
     * Render getCostOfPurchaseHtml example.
     */
    public function postProcess()
    {
        $paymentMethod = isset($_REQUEST['paymentmethod']) ? $_REQUEST['paymentmethod'] : null;
        $amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : null;
        if (!is_null($paymentMethod) && $amount > 0) {
            try {
                // Prepare ecom for extra content.
                $this->ecom->setCostOfPurcaseHtmlBefore($this->getAnnuityHtmlBefore());
                $this->ecom->setCostOfPurcaseHtmlAfter('</div>');
                // Fetch and display cost example from Resurs Bank.
                echo $this->ecom->getCostOfPurchase(
                    $paymentMethod,
                    $amount,
                    true,
                    $this->getModule()->getBasePath() . 'views/css/costofpurchase.css'
                );
            } catch (\Exception $e) {
                // Just tell them what's wrong.
                echo $e->getMessage();
            }
        } else {
            // As this is only a simple page of content, we don't need anything more complex than this.
            header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad request');
            die($this->getModule()->l('No payment method or amount found!'));
        }

        die;
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}