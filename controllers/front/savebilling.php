<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutSaveBillingModuleFrontController
 *
 * Saves the billing address information, provided in the iframe, in our session.
 */
class ResursBankCheckoutSaveBillingModuleFrontController extends ModuleFrontController
{
    /**
     * Use SSL if possible.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Collect customer billing address information from request and store in session.
     */
    public function postProcess()
    {
        try {
            // Collect and store customer information from submitted belling address.
            \ResursBankCheckout\Models\Service\Customer::handleDataRequest();

            // Collect and store billing address information.
            \ResursBankCheckout\Models\Service\Customer\Address\Billing::handleDataRequest();
        } catch (Exception $e) {
            // Log error.
            $this->getModule()->log($e, $_REQUEST);

            // Display error message.
            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while storing the address information. Please try again.')
            );
        }

        $this->getAjax()->respond();
    }

    /**
     * @return \ResursBankCheckout\Models\Controller\Ajax
     */
    protected function getAjax()
    {
        return $this->getModule()->getAjaxController($this);
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}
