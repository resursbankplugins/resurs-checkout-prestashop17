<?php
/**
 * Copyright 2019 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutCartControllerModuleFrontController
 */
class ResursBankCheckoutCartModuleFrontController extends ModuleFrontController
{
    /**
     * ResursBankCheckoutCostOfPurchaseModuleFrontController constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Collect payment method from request and store in session.
     */
    public function postProcess()
    {
        $hasProducts = false;
        $ajax = $this->getModule()->getAjaxController($this);

        if (isset($_REQUEST['action']) && $_REQUEST['action'] === 'checkcart') {
            $cart = $this->getModule()->getContext()->cart;
            if ($cart->hasProducts()) {
                $hasProducts = true;
                $ajax->addResponseData('go', null);
            } else {
                $ajax->addResponseData('go', $this->getModule()->getContext()->link->getPageLink('cart'));
            }
        }
        $ajax->addResponseData('hasProducts', $hasProducts);
        $ajax->respond();
        die;
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}