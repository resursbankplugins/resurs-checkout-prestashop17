<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use ResursBankCheckout\Models\Service\Customer\Address\Billing;
use ResursBankCheckout\Models\Service\Customer\Address\Shipping;

/**
 * Class ResursBankCheckoutSaveShippingModuleFrontController
 *
 * Saves the shipping address information, provided in the iframe, in our session.
 */
class ResursBankCheckoutSaveShippingModuleFrontController extends ModuleFrontController
{
    /**
     * Use SSL if possible.
     *
     * @var bool
     */
    public $ssl = true;

    /**
     * Collect customer shipping address information from request and store in session.
     */
    public function postProcess()
    {
        try {
            /*\ResursBankCheckout\Models\Service\Session::set(
                'resursbank_changed_shipping',
                true
            );*/

            // Store submitted shipping address information.
            Shipping::handleDataRequest(
                Billing::getSessionData('telephone')
            );

            $data = Shipping::getRequestData();
            $useForShipping = (bool)isset($data['use_for_shipping']) ? $data['use_for_shipping']: false;

            // Flag that we have a separate shipping address now.
            Billing::setUseForShipping($useForShipping);
        } catch (Exception $e) {
            // Log error.
            $this->getModule()->log($e, $_REQUEST);

            // Display generic error message.
            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while storing the shipping address information. Please try again.')
            );
        }

        $this->getAjax()->respond();
    }

    /**
     * @return \ResursBankCheckout\Models\Controller\Ajax
     */
    protected function getAjax()
    {
        return $this->getModule()->getAjaxController($this);
    }

    /**
     * @return \ResursBankCheckout
     */
    protected function getModule()
    {
        return $this->module;
    }
}
