<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class ResursBankCheckoutSaveShippingModuleFrontController
 *
 * Saves the shipping address information, provided in the iframe, in our session.
 */
class ResursBankCheckoutSaveDeliveryModuleFrontController extends ModuleFrontController
{
    /** @var ResursBankCheckout */
    public $module;

    /**
     * Collect customer shipping address information from request and store in session.
     */
    public function postProcess()
    {
        try {
            if (Tools::getIsset('delivery_id')) {
                // cart->id_address_delivery needs to be anything but 0 if we
                // want to set a delivery option on orders. If it's 0, we
                // store the user's choice in the session and update the
                // delivery option during order placement.
                if ((int)$this->context->cart->id_address_delivery === 0) {
                    \ResursBankCheckout\Models\Service\Session::set(
                        'resursbank_checkout_delivery_id',
                        Tools::getValue('delivery_id')
                    );
                }

                // Delivery option is always updated here, regardless if
                // cart->id_address_delivery has been set or not, to update
                // delivery prices on the checkout page.
                $this->context->cart->setDeliveryOption([
                    $this->context->cart->id_address_delivery =>
                        Tools::getValue('delivery_id')
                ]);

                $this->context->cart->update();
            }
        } catch (Exception $e) {
            // Log error.
            $this->module->log($e, $_REQUEST);

            // Display generic error message.
            $this->getAjax()->addError(
                $this->getModule()->l('Something went wrong while storing the shipping address information. Please try again.')
            );
        }
    }

    /**
     * Hook function that is fired after postProcess() when {ajax = true} in
     * request data.
     *
     * @throws PrestaShopException
     */
    public function displayAjax()
    {
        $cart = $this->cart_presenter->present(
            $this->context->cart
        );

        ob_end_clean();

        // Firing hooks before the AJAX call dies.
        $this->ajaxRender();

        $cartSummary = $this->render('checkout/_partials/cart-summary', array(
            'cart' => $cart,
            'static_token' => Tools::getToken(false),
        ));

        $ajax = $this->getAjax();
        $ajax->addResponseData('cart_summary', $cartSummary);
        $ajax->respond();

        return;
    }

    /**
     * @return \ResursBankCheckout\Models\Controller\Ajax
     */
    protected function getAjax()
    {
        return $this->module->getAjaxController($this);
    }
}
