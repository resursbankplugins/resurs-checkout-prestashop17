<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Foundation\Templating\RenderableProxy;

/**
 * Class ResursBankCheckoutSuccessModuleFrontController
 *
 * Display the success page after completing order placement and payment.
 */
class ResursBankCheckoutCheckoutModuleFrontController extends ModuleFrontController
{
    /**
     * @var CheckoutProcess
     */
    protected $checkoutProcess;

    /** @var ResursBankCheckout */
    public $module;

    public function initContent()
    {
        $this->checkoutProcess->handleRequest(
            Tools::getAllValues()
        );

        $this->checkoutProcess
            ->setNextStepReachable()
            ->markCurrentStep()
            ->invalidateAllStepsAfterCurrent();

        $deliveryOptions = $this->checkoutProcess
            ->getCheckoutSession()
            ->getDeliveryOptions();

        $selectedDeliveryOption = $this->checkoutProcess
            ->getCheckoutSession()
            ->getSelectedDeliveryOption();

        // Set the default selected shipping method if it exists among the
        // delivery options.
        if (isset($deliveryOptions[$selectedDeliveryOption])) {
            $this->context->cart->setDeliveryOption([
                $this->context->cart->id_address_delivery =>
                    $selectedDeliveryOption,
            ]);

            $this->context->cart->update();
        }

        $this->context->smarty->assign([
            'testValue' => 'TEST_VALUE',
            'checkout_process' => new RenderableProxy($this->checkoutProcess),
            'delivery_options' => $deliveryOptions,
            'selected_delivery_option' => $selectedDeliveryOption,
        ]);

        parent::initContent();

        $this->setTemplate(
            'module:resursbankcheckout/views/templates/front/checkout.tpl'
        );
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->registerJavascript(
            'rbc',
            'modules/resursbankcheckout/views/js/global/resursbank.js'
        );

        $this->registerJavascript(
            'rbc-classy',
            'modules/resursbankcheckout/views/js/global/classy/classy.js'
        );

        $this->registerJavascript(
            'rbc-user-address',
            'modules/resursbankcheckout/views/js/front/user-address.js'
        );

        $this->registerJavascript(
            'rbc-book-order',
            'modules/resursbankcheckout/views/js/front/book-order.js'
        );

        $this->registerJavascript(
            'rbc-payment-method',
            'modules/resursbankcheckout/views/js/front/payment-method.js'
        );

        $this->registerJavascript(
            'rbc-init',
            'modules/resursbankcheckout/views/js/front/checkout.js'
        );

        $this->registerJavascript(
            'rbc-error-handler',
            'modules/resursbankcheckout/views/js/front/error-handler.js'
        );

        $this->registerJavascript(
            'rbc-ajax-q',
            'modules/resursbankcheckout/views/js/front/ajax-q/ajax-q.js'
        );

        $this->registerJavascript(
            'rbc-ajax-q-chain',
            'modules/resursbankcheckout/views/js/front/ajax-q/ajax-q-chain.js'
        );

        $this->registerJavascript(
            'rbc-shipping-method',
            'modules/resursbankcheckout/views/js/front/delivery-method.js'
        );
    }

    public function postProcess()
    {
        parent::postProcess();

        $this->bootstrap();
    }

    protected function bootstrap()
    {
        $translator = $this->getTranslator();
        $session = $this->getCheckoutSession();

        $this->checkoutProcess = new CheckoutProcess(
            $this->context,
            $session
        );

        $checkoutDeliveryStep = new CheckoutDeliveryStep(
            $this->context,
            $translator
        );

        $this->checkoutProcess->addStep($checkoutDeliveryStep);
    }

    protected function getCheckoutSession()
    {
        $deliveryOptionsFinder = new DeliveryOptionsFinder(
            $this->context,
            $this->getTranslator(),
            $this->objectPresenter,
            new PriceFormatter()
        );

        $session = new CheckoutSession(
            $this->context,
            $deliveryOptionsFinder
        );

        return $session;
    }
}
