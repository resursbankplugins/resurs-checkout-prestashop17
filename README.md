# Resurs Bank Checkout for Prestashop 1.7

Find out more from the documentation at https://test.resurs.com/docs/display/ecom/PrestaShop+Payment+Gateways

## Prerequisites

* PrestaShop 1.7.6-1.7.7 (tested).
* For Internet Explorer users: Version 9 or later is required.
* Compatible with PHP 7.0 and higher.
* PHP module for curl (For REST-services)
* PHP module for xml+soapclient (For web-services/WSDL)
* At least PHP 5.6: As of 2.7.1, the module is no longer compatible with PHP 5.3
* SSL based library (For outbound communication **with** Resurs Bank)

## Upgrade

For each upgrade you do, make sure you clear all cache data in PrestaShop admin (Via the Advanced->Performance tab).

## Installation

Download our module from https://bitbucket.org/resursbankplugins/resurs-checkout-prestashop17/downloads/?tab=tags
If there are no present tag, for prestashop 1.7 - go
check https://bitbucket.org/resursbankplugins/resurs-checkout-prestashop17/downloads/?tab=downloads.

1. For both Prestashop 1.6 and 1.7, grab a tagged release from our repos (do not use the master-branch as this usually
   is a development branch - and even if it's for the moment stable, we can't guarantee full production support at this
   level).
2. Unzip the downloaded file into <primary folder of your shop>/modules (the downloaded file may look like this:
   resursbankplugins-resurs-checkout-prestashop16-ff8652e77e83.zip)
3. Make sure you rename the primary directory that you unzipped to the modules directory. If the zip looks like above (
   resursbankplugins-resurs-checkout-prestashop16-ff8652e77e83.zip), the directory that the module will land under may
   look like this: resursbankplugins-resurs-checkout-prestashop16-ff8652e77e83
4. Make sure your cloned or extracted directory under modules is renamed to resursbankcheckout, or the module won't be
   found by prestashop therefore not work.
5. Activate the module and configure it through the admin module panel. Do not forget to put the username and password
   in the credentials field.
6. Also do not forget to register payment methods and callbacks (see screen dump below)!
7. Before going production mode, make sure you test the module so you know it covers your needs!

### Cloning the repository

You can also clone the respository like the below example, if you are used to use for example SSH. Make sure your
directory location is in prestashop-root/modules.

    git clone https://bitbucket.org/resursbankplugins/resurs-checkout-prestashop17.git resursbankcheckout

### How we deploy ecomphp

Sample script of how we deploy new ecom-releases:

    #!/bin/bash
    echo "Preparing library structure..."
    echo "Clean up cache ..."
    composer clear >/dev/null 2>&1
    echo "Upgrading vendor by removal..."
    rm -fr vendor
    composer install --prefer-dist
    composer up
    echo "Cleaning up unnecessary ignores ..."
    find vendor/ -name .gitignore -exec rm -vf {} \; >/dev/null 2>&1
    echo "Cleaning up .git structures ..."
    find vendor/ -type d -name .git -exec rm -rvf {} \; >/dev/null 2>&1
    echo "Cleaning up tests..."
    find vendor/ -type d -name tests -exec rm -rvf {} \; >/dev/null 2>&1

## Changelog

See [CHANGELOG.md](CHANGELOG.md)
