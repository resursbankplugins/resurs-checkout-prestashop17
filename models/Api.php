<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models;

use Cart;
use Configuration;
use Exception;
use Order;
use Resursbank\RBEcomPHP\RESURS_ENVIRONMENTS;
use Resursbank\RBEcomPHP\RESURS_FLOW_TYPES;
use Resursbank\RBEcomPHP\ResursBank;
use Resursbank\RBEcomPHP\ResursBank as ECom;
use ResursBankCheckout;
use ResursBankCheckout\Models\Api\Callback as CallbackHandler;
use ResursBankCheckout\Models\Exception\MissingDataException;
use ResursBankCheckout\Models\Items\Cart as CartItems;
use ResursBankCheckout\Models\Items\Order as OrderItems;
use ResursBankCheckout\Models\Log\Api as LogHandler;
use ResursBankCheckout\Models\Service\Cart as CartService;
use ResursBankCheckout\Models\Service\Config;
use ResursBankCheckout\Models\Service\Session;
use TorneLIB\Module\Config\WrapperConfig;
use TorneLIB\Module\Network\NetWrapper;

/**
 * Class Api
 *
 * Methods to communicate with the Resurs Bank payment gateway.
 *
 * @package ResursBankCheckout\Models
 */
class Api
{
    /**
     * URL to test environment.
     */
    const TEST_ENVIRONMENT_URL = 'https://omnitest.resurs.com';

    /**
     * URL to production environment.
     */
    const PRODUCTION_ENVIRONMENT_URL = 'https://checkout.resurs.com';

    /**
     * Key in checkout/session where we store the payment session id provided by the API.
     */
    const PAYMENT_SESSION_ID_KEY = 'resursbank_checkout_payment_session_id';

    /**
     * Key in checkout/session where we store the resulting iframe provided by the API when we initialize a new session.
     */
    const PAYMENT_SESSION_IFRAME_KEY = 'resursbank_checkout_payment_session_iframe';

    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * @var CallbackHandler
     */
    private $callbacksModel;

    /**
     * ECom connection.
     *
     * @var ECom
     */
    private $ecomConnection;

    /**
     * @var LogHandler
     */
    private $logHandler;

    /**
     * Api constructor.
     *
     * @param ResursBankCheckout $module
     */
    public function __construct(ResursBankCheckout $module)
    {
        $this->module = $module;
    }

    /**
     * Create a new payment session. Only viable on frontend since it fetches initial order lines statically from the
     * cart instance stored in our request context.
     *
     * @return string
     * @throws Exception
     */
    public function createPayment()
    {
        $result = '';

        try {
            $data = [
                "orderLines" => $this->getPaymentLinesFromCart(
                    $this->module->getContext()->cart
                ),
                "shopUrl" => $this->module->getBaseUrl(),
            ];

            $this->ecom()->setSigning(
                $this->getSuccessUrl(),
                $this->getBackUrl(),
                false,
                null
            );

            $allowPrefilledCustomer = false;
            if ($allowPrefilledCustomer) {
                $customer = $this->module->getContext()->customer;
                // Quick on- and off switching.
                if ($this->module->getContext()->customer->isLogged()) {
                    // Hämta adress + skicka till setDelivery.
                    $simpleAddressList = $customer->getAddresses($customer->id_lang);
                    if (count($simpleAddressList)) {
                        $firstMatch = array_shift($simpleAddressList);
                        $countryId = \Tools::getCountry($firstMatch);
                        $isoCountry = \Country::getIsoById($countryId);
                        $this->ecom()->setCustomer(
                            '',
                            '',
                            $this->getDataFromCustomerObject('phone', $firstMatch),
                            $customer->email,
                            'NATURAL',
                            ''
                        );
                        $this->ecom()->setDeliveryAddress(
                            sprintf(
                                '%s %s',
                                $this->getDataFromCustomerObject('firstname', $firstMatch),
                                $this->getDataFromCustomerObject('lastname', $firstMatch)
                            ),
                            $this->getDataFromCustomerObject('firstname', $firstMatch),
                            $this->getDataFromCustomerObject('lastname', $firstMatch),
                            $this->getDataFromCustomerObject('address1', $firstMatch),
                            $this->getDataFromCustomerObject('address2', $firstMatch),
                            $this->getDataFromCustomerObject('city', $firstMatch),
                            $this->getDataFromCustomerObject('postcode', $firstMatch),
                            $isoCountry
                        );
                    }
                }
            }

            $result = $this->ecom()->createPayment(
                $this->getPaymentId(true),
                $data
            );

            $ecomPayload = $this->ecom()->getPayload();

            // Create log entry for debug purposes.
            $this->log('Created payment session.', [
                'parameters' => $data,
                'result' => $result,
            ]);
            $this->log('Payment session ecom payload content', [
                'payload' => $ecomPayload,
            ]);
        } catch (Exception $e) {
            $this->log($e, (isset($data) ? $data : []));
            throw $e;
        }

        return is_string($result) ? $result : '';
    }

    /**
     * Fetch customer data from Customer Address block.
     * @param $key
     * @param $customerObject
     * @return mixed|string
     */
    private function getDataFromCustomerObject($key, $customerObject)
    {
            return isset($customerObject[$key]) ? $customerObject[$key] : '';
    }

    /**
     * Update existing payment.
     *
     * @param string $id
     * @param array $lines
     * @return array
     * @throws Exception
     */
    public function updatePayment($id, array $lines)
    {
        $result = [];

        try {
            $result = $this->ecom()->updateCheckoutOrderLines((string)$id, $lines);

            // Create log entry for debug purposes.
            $this->log('Updated payment session.', [
                'id' => $id,
                'payment_lines' => $lines,
                'result' => $result,
            ]);
        } catch (Exception $e) {
            $this->log($e, [
                'id' => $id,
                'lines' => $lines,
            ]);

            // If internal server errors are generated here, it is most likely Resurs Bank that throws the error.
            // When this occurs in the product view, it may be ok to ignore it.
            if ($e->getCode() < 500) {
                throw $e;
            }
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Update payment reference.
     *
     * @param string $oldId
     * @param string $newId
     * @return bool
     * @throws Exception
     */
    public function updatePaymentReference($oldId, $newId)
    {
        $result = false;

        try {
            $result = $this->ecom()->updatePaymentReference(
                (string)$oldId,
                (string)$newId
            );

            // Create log entry for debug purposes.
            $this->log('Updated payment reference.', [
                'old_id' => $oldId,
                'new_id' => $newId,
            ]);
        } catch (Exception $e) {
            $this->log($e, [
                'oldId' => $oldId,
                'newId' => $newId,
            ]);

            throw $e;
        }

        return (bool)$result;
    }

    /**
     * Check if a payment session has been initiated.
     *
     * @return bool
     */
    public function paymentSessionInitiated()
    {
        return $this->getPaymentId() !== '';
    }

    /**
     * Clear session data.
     *
     * @return $this
     */
    public function clearSession()
    {
        Session::uns(self::PAYMENT_SESSION_IFRAME_KEY);
        Session::uns(self::PAYMENT_SESSION_ID_KEY);

        return $this;
    }

    /**
     * Retrieve callbacks model.
     *
     * @return CallbackHandler
     */
    public function getCallbackHandler()
    {
        if ($this->callbacksModel === null) {
            $this->callbacksModel = new CallbackHandler($this);
        }

        return $this->callbacksModel;
    }

    /**
     * Retrieve module instance.
     *
     * @return ResursBankCheckout
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Fetch iframe instance from API and store within session for repeated
     * usage (when re-rendering checkout page etc.).
     *
     * @return string
     * @throws Exception
     */
    public function getIframe()
    {
        $result = Session::get(self::PAYMENT_SESSION_IFRAME_KEY);

        if ($result === null) {
            // Create new payment session.
            $result = $this->createPayment();

            // Store payment session in local session.
            Session::set(self::PAYMENT_SESSION_IFRAME_KEY, $result);
        }

        return (string)$result;
    }

    /**
     * Retrieve payment Id.
     *
     * @param bool $refresh
     * @return string
     */
    public function getPaymentId($refresh = false)
    {
        if ($refresh) {
            Session::set(self::PAYMENT_SESSION_ID_KEY, $this->module->strRand(32));
        }

        return (string)Session::get(self::PAYMENT_SESSION_ID_KEY);
    }

    /**
     * Retrieve back URL.
     *
     * @return string
     */
    public function getBackUrl()
    {
        return (string)$this->module->getSimpleLink(
            'failure',
            [
                'cart_id' => $this->module->getContext()->cart->id,
                'validation_key' => CartService::getValidationKey(
                    $this->module->getContext()->cart
                ),
            ]
        );
    }

    /**
     * Construct and return the success page URL.
     *
     * @return string
     */
    public function getSuccessUrl()
    {
        return (string)$this->module->getSimpleLink('success', [
            'id_cart' => $this->module->getContext()->cart->id,
        ]);
    }

    /**
     * Retrieve payment lines, used in API call payloads, from a shopping cart.
     *
     * @param Cart $cart
     * @return array
     * @throws Exception
     */
    public function getPaymentLinesFromCart(Cart $cart)
    {
        $driver = new CartItems($this->module);

        return $driver->getPaymentLines($cart);
    }

    /**
     * Retrieve payment lines, used in API call payloads, from an order.
     *
     * @param Order $order
     * @return array
     * @throws Exception
     */
    public function getPaymentLinesFromOrder(Order $order)
    {
        $driver = new OrderItems($this->module);

        return $driver->getPaymentLines($order);
    }

    /**
     * Retrieve API environment (test/production).
     *
     * @return int
     */
    public function getEnvironment()
    {
        return (int)Configuration::get(Config::SETTING_ENVIRONMENT) === 1 ?
            RESURS_ENVIRONMENTS::PRODUCTION :
            RESURS_ENVIRONMENTS::TEST;
    }

    /**
     * Retrieve API URL.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getEnvironment() === RESURS_ENVIRONMENTS::PRODUCTION ?
            self::PRODUCTION_ENVIRONMENT_URL :
            self::TEST_ENVIRONMENT_URL;
    }

    /**
     * Check if credentials are valid that are not saved in the database.
     *
     * @param bool $isProduction
     * @param string $username
     * @param string $password
     * @return  bool
     * @throws Exception
     */
    public function checkCredentials($isProduction, $username, $password)
    {
        $env = $isProduction ? RESURS_ENVIRONMENTS::PRODUCTION : RESURS_ENVIRONMENTS::TEST;

        $ecom = new ECom($username, $password, $env);

        $ecom->setPreferredPaymentFlowService(RESURS_FLOW_TYPES::RESURS_CHECKOUT);
        $ecom->setUserAgent(
            $this->module->getName() . "-" . $this->module->getVersion() . "/PrestaShop-" . _PS_VERSION_
        );

        try {
            $ecom->getPaymentMethods();
        } catch (\Exception $e) {
            if ($e->getCode() == 401) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validate API credentials that are stored.
     *
     * @return bool
     * @throws Exception
     */
    public function validateCredentials()
    {
        $credentials = $this->getEcomCredentials();
        $return = false;
        try {
            $return = $this->ecom()->validateCredentials(
                $this->getEnvironment() === RESURS_ENVIRONMENTS::PRODUCTION ? 0 : 1,
                $credentials['username'],
                $credentials['password']
            );
        } catch (\Exception$e) {
        }
        return $return;
    }

    /**
     * Get API credentials.
     *
     * @return array
     */
    public function getEcomCredentials()
    {
        $environment = $this->getEnvironment() === RESURS_ENVIRONMENTS::PRODUCTION ? 'LIVE' : 'TEST';

        return [
            'username' => (string)Configuration::get('RESURSBANKCHECKOUT_' . $environment . '_ACCOUNT_USERNAME'),
            'password' => (string)Configuration::get('RESURSBANKCHECKOUT_' . $environment . '_ACCOUNT_PASSWORD'),
        ];
    }

    /**
     * Retrieve connection to ECom.
     *
     * @return ECom
     * @throws Exception
     */
    public function ecom()
    {
        if ($this->ecomConnection === null) {
            $this->ecomConnection = $this->getEcomConnection();
        }

        return $this->ecomConnection;
    }

    /**
     * Create ECom connection.
     *
     * @return ECom
     * @throws Exception
     */
    public function getEcomConnection()
    {
        // Retrieve API credentials.
        $credentials = $this->getEcomCredentials();

        // Create connection.
        $connection = new ECom(
            $credentials['username'],
            $credentials['password'],
            $this->getEnvironment()
        );
        $connection->setWsdlCache(true);
        $connection->setPreferredPaymentFlowService(RESURS_FLOW_TYPES::RESURS_CHECKOUT);

        // Disable instant finalization controls (for swish, etc) for the current version.
        $connection->setAutoDebitableTypes(false);

        $uAgent = [
            sprintf('%s-%s', $this->module->getName(), $this->module->getVersion()),
            sprintf('EComPHP-%s', (new ResursBank())->getVersionNumber()),
            sprintf('NETCURL-%s', (new NetWrapper())->getVersion()),
            sprintf('PrestaShop-%s', _PS_VERSION_),
            sprintf('PHP-%s', PHP_VERSION),
        ];
        $currentSignature = WrapperConfig::getSignature();
        if (empty($currentSignature)) {
            WrapperConfig::setSignature(implode('/', $uAgent));
        }
        //$connection->setUserAgent(implode('/', $uAgent));

        return $connection;
    }

    /**
     * Retrieve iframe protocol:domain. This is used for iframe communication (from JavaScript). This follows the same
     * rules as getShopUrl() above, so no trailing slashes (e.g https://resursbank.com)
     *
     * @return string
     */
    public function getIframeUrl()
    {
        return rtrim($this->getUrl(), '/');
    }

    /**
     * Create log entry.
     *
     * @param string|Exception $message
     * @param array $context
     * @return $this
     * @throws Exception
     */
    public function log($message, array $context = [])
    {
        $this->getLogHandler()->log($message, $context);

        return $this;
    }

    /**
     * Retrieve log handler.
     *
     * @return LogHandler
     */
    private function getLogHandler()
    {
        if ($this->logHandler === null) {
            $this->logHandler = new LogHandler();
        }

        return $this->logHandler;
    }

    /**
     * Retrieve all available payment methods from the API.
     *
     * @return array
     * @throws Exception
     */
    public function getPaymentMethods()
    {
        $result = $this->ecom()->getPaymentMethods();

        return is_array($result) ? $result : [];
    }

    /**
     * Retrieve customer SSN from a payment.
     *
     * @param string $reference
     * @return string
     * @throws Exception
     * @throws MissingDataException
     */
    public function getPaymentSsn($reference)
    {
        $payment = $this->ecom()->getPayment($reference);

        if (!is_object($payment) ||
            !isset($payment->customer) ||
            !is_object($payment->customer) ||
            !isset($payment->customer->governmentId)
        ) {
            throw new MissingDataException(
                sprintf(
                    'Payment %s is missing a valid SSN.',
                    $reference
                ),
                4004
            );
        }

        return (string)$payment->customer->governmentId;
    }

    /**
     * Request annuity factors for specific payment method.
     *
     * @param $paymentMethodId
     * @return array
     * @throws Exception
     */
    public function getAnnuityFactors($paymentMethodId)
    {
        $result = $this->ecom()->getAnnuityFactors($paymentMethodId);

        return is_array($result) ? $result : [];
    }
}
