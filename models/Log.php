<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models;

use \Exception;
use \Configuration;
use \AbstractLoggerCore;
use \FileLogger;
use \ResursBankCheckout\Models\Service\Config;

/**
 * Class Log
 *
 * General log handler.
 *
 * @package ResursBankCheckout\Models
 */
class Log
{
    /**
     * @var string
     */
    protected $filename = 'general';

    /**
     * @var bool
     */
    private $active;

    /**
     * @var FileLoggerCore
     */
    private $logger;

    /**
     * Log constructor.
     */
    public function __construct($filename = null)
    {
        if ($filename !== null) {
            $this->filename = $filename;
        }
        $this->init();
    }

    /**
     * Initialize log handler.
     *
     * @return $this
     */
    protected function init()
    {
        $this->logger = new FileLogger(AbstractLoggerCore::DEBUG);
        $this->logger->setFilename(_PS_ROOT_DIR_ . '/var/logs/resursbank_' . $this->filename . '.log');

        return $this;
    }

    /**
     * Create log message entry if debug mode is enabled.
     *
     * @param string|Exception $message
     * @param array $context
     * @return $this
     * @throws Exception
     */
    public function log($message, array $context = array())
    {
        if ($this->isActive()) {
            $this->entry($message, $context);
        }

        return $this;
    }

    /**
     * Create log message entry.
     *
     * @param string|Exception $message
     * @param array $context
     * @return $this
     * @throws Exception
     */
    public function entry($message, array $context = array())
    {
        // Collect message data from Exception object.
        if ($message instanceof Exception) {
            $message = "\n\n" .
                ($message->getFile() . ' :: ' . $message->getLine()) .
                (' [ ' . $message->getMessage() . ' ] ') .
                "\n\n" .
                $message->getTraceAsString();
        }

        if (!is_string($message)) {
            throw new Exception('We cannot log data which is not either a string or an Exception.');
        }

        // Append context to message.
        if (count($context)) {
            $message .= "\n\n" . @json_encode($context);
        }

        // Append separator.
        $message .= "\n\n--------------------\n";

        // Log message.
        $this->logger->logDebug($message);

        return $this;
    }

    /**
     * Check whether or not debugging mode is enabled.
     *
     * @return bool
     */
    public function isActive()
    {
        if ($this->active === null) {
            $this->active = (bool) Configuration::get(Config::SETTING_DEBUG_ENABLED);
        }

        return $this->active;
    }
}
