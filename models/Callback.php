<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models;

use \Exception;
use \Order;
use \ResursBankCheckout;
use \Configuration;
use \ResursBankCheckout\Models\Service\Config;
use \Resursbank\RBEcomPHP\RESURS_PAYMENT_STATUS_RETURNCODES;
use \ResursBankCheckout\Models\Service\Order as OrderService;
use \ResursBankCheckout\Models\Service\Order\State;
use \ResursBankCheckout\Models\Log\Callback as LogHandler;
use \ResursBankCheckout\Models\Service\Customer\Address\Billing;
use \ResursBankCheckout\Models\Exception\MissingDataException;

/**
 * Class Callback
 *
 * General Callback parent class. When we receive an incoming callback we create
 * an instance of its associated Callbacks\[Event] class. These classes all
 * extend this class where we keep general methods for common callback
 * operations.
 *
 * @package ResursBankCheckout\Models
 */
class Callback
{
    /**
     * @var ResursBankCheckout
     */
    protected $module;

    /**
     * @var LogHandler
     */
    private $logHandler;

    /**
     * Callback event, in uppercase (BOOKED, UNFREEZE etc.).
     *
     * @var string
     */
    protected $event;

    /**
     * Callback constructor.
     *
     * @param ResursBankCheckout $module
     */
    public function __construct(ResursBankCheckout $module)
    {
        $this->module = $module;
    }

    /**
     * General callback execution instructions.
     *
     * @throws Exception
     */
    public function execute()
    {
        // Validate digest etc. to ensure callback is valid.
        $this->validate();

        try {
            // Update missing SSN on billing address.
            $this->updateSsn();
        } catch (MissingDataException $e) {
            // In case no SSN exists on the payment at Resurs Bank we simply
            // want to log this and keep going, it's not a fatal error.
            $this->log($e, $_REQUEST);
        }

        // Update order status, unless we should annul the order.
        if ($this->getEvent() !== 'ANNULMENT') {
            $this->updateOrderStatus();
        } else {
            $this->cancelOrder();
        }
    }

    /**
     * Retrieve generated SALT key.
     *
     * @return mixed
     */
    public function getSaltKey()
    {
        return Configuration::get(Config::SETTING_SALT);
    }

    /**
     * Retrieve request parameter value.
     *
     * @param string $key
     * @param null|string $default
     * @return mixed
     */
    protected function getParam($key, $default = null)
    {
        return isset($_REQUEST[$key]) ?
            $_REQUEST[$key] :
            $default;
    }

    /**
     * Validate callback request.
     *
     * @return Callback
     * @throws Exception
     */
    protected function validate()
    {
        $digest = strtoupper(sha1(
            $this->getParam('paymentId') .
            $this->getParam('result', '') .
            $this->getSaltKey()
        ));

        if ($digest !== $this->getParam('digest')) {
            throw new Exception('Invalid digest.');
        }

        return $this;
    }

    /**
     * Retrieve order from request paymentId.
     *
     * @return Order
     * @throws Exception
     */
    protected function getOrderByPaymentId()
    {
        $id = $this->getParam('paymentId');

        $order = null;

        // Retrieve collection of all orders with the provided reference.
        $orders = Order::getByReference($id);

        // If there were more than one order with this reference we cannot be
        // sure which to use, so we stop.
        if (count($orders) === 1) {
            foreach ($orders as $o) {
                $order = $o;
            }
        }

        // Validate order.
        if (!($order instanceof Order) ||
            !isset($order->id) ||
            (int)$order->id === 0
        ) {
            throw new Exception('Failed to load order using payment id ' . $id);
        }

        return $order;
    }

    /**
     * Retrieve Resurs Bank payment status.
     *
     * @return int
     * @throws Exception
     */
    protected function getPaymentStatus()
    {
        return (int)$this->ecom()
            ->getOrderStatusByPayment(
                $this->getParam('paymentId'),
                $this->ecom()->getCallbackTypeByString(
                    $this->getEvent()
                ),
                $this->getParam('result', '')
            );
    }

    /**
     * Retrieve order status from Resurs Bank payment.
     *
     * @return int
     * @throws Exception
     */
    protected function getOrderStatusFromPayment()
    {
        $result = 0;

        // If the event automatic_fraud_control comes in with result "frozen"
        // we force the order status to "suspected fraud", otherwise the status
        // will be determined by the payment status.
        if ($this->getEvent() === 'AUTOMATIC_FRAUD_CONTROL' &&
            strtoupper($this->getParam('result')) === 'FROZEN'
        ) {
            $result = Configuration::get(State::SUSPECTED_FRAUD);
        } else {
            // Retrieve payment status.
            $paymentStatus = $this->getPaymentStatus();

            // Retrieve appropriate status state based on payment status. Going bitwise as of v2.6.4.
            // Note: Currently not checking the RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_AUTOMATICALLY_DEBITED bit.
            switch (true) {
                case $paymentStatus & (RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_CREDITED):
                    $result = _PS_OS_REFUND_;
                    break;
                case $paymentStatus & (RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_ANNULLED):
                    $result = _PS_OS_CANCELED_;
                    break;
                case $paymentStatus & RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_COMPLETED:
                    $result = Configuration::get(State::COMPLETED);
                    break;
                case $paymentStatus & RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_PROCESSING:
                    $result = Configuration::get(State::PROCESSING);
                    break;
            }
        }

        return (int)$result;
    }

    /**
     * Updates the status of the order referenced by paymentId in the request
     * (if necessary).
     *
     * @return $this
     * @throws Exception
     */
    protected function updateOrderStatus()
    {
        /** @var Order $order */
        $order = $this->getOrderByPaymentId();

        OrderService::changeOrderState(
            $order,
            $this->getOrderStatusFromPayment()
        );

        return $this;
    }

    /**
     * Retrieve ECom library instance.
     *
     * @return \Resursbank\RBEcomPHP\ResursBank
     * @throws Exception
     */
    protected function ecom()
    {
        return $this->module->getApi()->ecom();
    }

    /**
     * Create log entry.
     *
     * @param string|Exception $message
     * @param array $context
     * @return $this
     * @throws Exception
     */
    public function log($message, array $context = array())
    {
        $this->getLogHandler()->log($message, $context);

        return $this;
    }

    /**
     * Retrieve log handler.
     *
     * @return LogHandler
     */
    private function getLogHandler()
    {
        if ($this->logHandler === null) {
            $this->logHandler = new LogHandler();
        }

        return $this->logHandler;
    }

    /**
     * Update SSN on billing address if it's missing. It will be missing if
     * the client didn't use their SSN to fetch address information during
     * the checkout process.
     *
     * @return $this
     * @throws Exception
     */
    public function updateSsn()
    {
        Billing::updateMissingData(
            $this->module,
            $this->getOrderByPaymentId()
        );

        return $this;
    }

    /**
     * Retrieve requested callback action from $_REQUEST.
     *
     * @return string
     * @throws Exception
     */
    public function getEvent()
    {
        if ($this->event === null) {
            // Get event from $_REQUEST.
            $event = isset($_REQUEST['event']) ?
                (string)$_REQUEST['event'] :
                '';

            // We need an event to continue.
            if ($event === '') {
                throw new Exception('No callback event requested.');
            }

            // Clean up event name.
            $this->event = (string)preg_replace(
                '/[^A-Z\_]/',
                '',
                strtoupper($event)
            );
        }

        return (string)$this->event;
    }

    /**
     * Cancel order.
     *
     * @return $this
     * @throws Exception
     */
    protected function cancelOrder()
    {
        OrderService::cancel($this->getOrderByPaymentId());

        return $this;
    }
}
