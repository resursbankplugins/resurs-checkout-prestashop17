<?php

namespace ResursBankCheckout\Models\Exception;

use \Exception;

/**
 * Class MissingDataException
 *
 * This Exception class is specifically meant to indicate missing data.
 *
 * @package Resursbank\Checkout\Models\Exception
 */
class MissingDataException extends Exception
{

}
