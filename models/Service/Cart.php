<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use Address as PrestaAddress;
use Cart as PrestaCart;
use CartRule;
use Configuration;
use Customer as PrestaCustomer;
use Exception;
use Order as PrestaOrder;
use PrestaShopException;
use ResursBankCheckout;
use ResursBankCheckout\Models\Api;
use ResursBankCheckout\Models\Service\Customer\Address\Billing;
use ResursBankCheckout\Models\Service\Customer\Address\Shipping;
use Tools;

/**
 * Class Cart
 *
 * Cart service. Contains methods to handle cart operations and converting a cart into an order.
 *
 * @package ResursBankCheckout\Models\Service
 */
abstract class Cart
{
    /**
     * Create order from cart.
     *
     * @param ResursBankCheckout $module
     * @throws Exception
     */
    public static function place(ResursBankCheckout $module)
    {
        // Validate payment session before we begin.
        self::validatePaymentSession($module->getApi());

        /** @var PrestaCustomer $customer */
        $customer = self::resolveCustomer($module, Customer::getSessionData());

        /** @var PrestaAddress $billingAddress */
        $billingAddress = self::resolveBillingAddress(
            $customer,
            $module->getContext()->cart->id_lang
        );

        try {
            /** @var PrestaAddress|null $shippingAddress */
            $shippingAddress = self::resolveShippingAddress(
                $customer,
                $module->getContext()->cart->id_lang
            );
        } catch (Exception $e) {
            // This value has to be set, otherwise prepareForOrderPlacement will be confused as $shippinAddress
            // will enter that method as null or unExistent. So preprocessed corrected data will occur earlier
            // than intended.
            $shippingAddress = $billingAddress;
        }

        // Update cart information before we place order.
        self::prepareForOrderPlacement(
            $module->getContext()->cart,
            $customer,
            $billingAddress,
            $shippingAddress,
            $module
        );

        // By default, cart->id_address_delivery is 0, and we need it to be
        // anything but that, otherwise we cannot set the delivery option on
        // orders. Therefore, we store the user's choice in the session and
        // update the delivery option here, where we are sure that
        // id_address_delivery has been updated.
        if (Session::has('resursbank_checkout_delivery_id')) {
            $module->getContext()->cart->setDeliveryOption([
                $module->getContext()->cart->id_address_delivery =>
                    Session::get('resursbank_checkout_delivery_id')
            ]);
            $module->getContext()->cart->update();
        }

        // Place order.
        self::placeOrder(
            $module,
            $customer,
            $billingAddress,
            self::resolvePaymentMethod()
        );

        try {
            self::syncOrderDataToPayment($module);
        } catch (Exception $e) {
            $module->log($e, $_REQUEST);
        }

        // Clear out session data.
        self::clearSessionData($module);
    }

    /**
     * Validate payment session.
     *
     * @param Api $api
     * @throws Exception
     */
    private static function validatePaymentSession(Api $api)
    {
        // Make sure a payment session exist before we attempt to update it.
        if (!$api->paymentSessionInitiated()) {
            throw new Exception('No payment session has been created.', 802);
        }
    }

    /**
     * Sync order data (items as payment lines, reference as id) to Resurs Bank payment.
     *
     * @param ResursBankCheckout $module
     * @throws Exception
     */
    private static function syncOrderDataToPayment(ResursBankCheckout $module)
    {
        /** @var PrestaOrder $order */
        $order = Order::getByCart($module->getContext()->cart);

        // Update payment reference to coincide with order reference.
        self::updatePaymentReference($module->getApi(), $order);
    }

    /**
     * Update payment reference to coincide with order reference.
     *
     * @param Api $api
     * @param PrestaOrder $order
     * @throws Exception
     * @internal param ResursBankCheckout $module
     */
    private static function updatePaymentReference(
        Api $api,
        PrestaOrder $order
    ) {
        $api->updatePaymentReference(
            $api->getPaymentId(),
            $order->reference
        );
    }

    /**
     * Retrieve payment method name from session.
     *
     * @return string
     * @throws Exception
     */
    private static function resolvePaymentMethod()
    {
        $result = (string)Payment::getSessionData();

        return $result !== '' ? $result : 'Resurs Bank';
    }

    /**
     * Place order.
     *
     * @param ResursBankCheckout $module
     * @param PrestaCustomer $customer
     * @param PrestaAddress $billingAddress
     * @param $paymentMethod
     * @return bool
     * @throws Exception
     */
    private static function placeOrder(
        ResursBankCheckout $module,
        PrestaCustomer $customer,
        PrestaAddress $billingAddress,
        $paymentMethod
    ) {
        $result = null;
        $hasError = null;

        // Create order.
        try {
            $result = Order::create(
                $module,
                $module->getContext()->cart,
                $customer,
                $billingAddress,
                $paymentMethod
            );
        } catch (Exception $createOrderException) {
            if ($createOrderException->getCode() === 999) {
                throw $createOrderException;
            }
            $hasError = $createOrderException;
        }

        // Validate creation process.
        if (!$result) {
            $defaultMessage = is_null($hasError) ? 'Unknown error' : $hasError->getMessage();
            $errorMessage = sprintf(
                "Failed to create order: %s\nData set %s.",
                $defaultMessage,
                @json_encode(
                    [
                        'customer' => $customer->id,
                        'billingAddress' => $billingAddress->id,
                        'paymentMethod' => $paymentMethod,
                        'cart' => $module->getContext()->cart->id,
                    ]
                )
            );

            $errorCode = !is_null($hasError) && $hasError->getCode() !== 0 ? $hasError->getCode() : 998;

            throw new Exception(
                $errorMessage,
                $errorCode
            );
        }

        return $result;
    }

    /**
     * Attempt to load customer from the "email" property in the provided $data argument. If the customer could not be
     * found we create a new customer, using the data provided in the $data argument.
     *
     * @param ResursBankCheckout $module
     * @param array $data
     * @return PrestaCustomer
     * @throws Exception
     */
    private static function resolveCustomer(
        ResursBankCheckout $module,
        array $data
    ) {
        /** @var PrestaCustomer $customer */
        $customer = self::resolveCustomerFromContext($module);

        if ($customer === null) {
            $customer = self::resolveCustomerFromEmail($data);
        }

        // Create new customer if needed.
        if (!self::validatePrestaCustomer($customer)) {
            $customer = Customer::create($data);

            // Validate again.
            if (!self::validatePrestaCustomer($customer)) {
                throw new Exception('Failed to create new customer with data ' . @json_encode($data));
            }
        }

        return $customer;
    }

    /**
     * Attempt to resolve customer object from context. Basically this let us collect a logged in customer.
     *
     * @param ResursBankCheckout $module
     * @return PrestaCustomer|null
     */
    private static function resolveCustomerFromContext(ResursBankCheckout $module)
    {
        $result = null;

        if (($module->getContext()->customer instanceof PrestaCustomer) &&
            isset($module->getContext()->customer->id) &&
            (int)$module->getContext()->customer->id !== 0
        ) {
            $result = $module->getContext()->customer;
        }

        return $result;
    }

    /**
     * Resolve existing customer account by provided email address.
     *
     * @param array $data
     * @return PrestaCustomer
     * @throws Exception
     */
    private static function resolveCustomerFromEmail(array $data)
    {
        if (!isset($data['email'])) {
            throw new Exception('Email missing in customer session data.', 803);
        }

        // Load customer by email.
        return Customer::getByEmail((string)$data['email']);
    }

    /**
     * Retrieve billing address attached to $customer. If no address was found, create one. If an address was found,
     * update it with the provided billing information and save it.
     *
     * @param PrestaCustomer $customer
     * @param int $languageId
     * @return PrestaAddress
     * @throws Exception
     */
    private static function resolveBillingAddress(
        PrestaCustomer $customer,
        $languageId
    ) {
        /** @var array $data */
        $data = Billing::getSessionData();

        if (!is_array($data) || !count($data)) {
            throw new Exception('No billing address data in session.');
        }

        // Get address collection from customer.
        $addressCollection = Billing::getPrestaAddressCollection(
            $customer,
            (int)$languageId
        );

        /** @var PrestaAddress $address */
        $address = new PrestaAddress();

        // Ignore this section completely if not explicitly required.
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            // To sanitize initial information from $data, we for some reason have to sanitize the data first,
            // before saving the data back to the $data object if we don't want unrelated exceptions.
            $sanitizedNameArray = self::getSanitizedNames($data);
            $data = $sanitizedNameArray;
        }

        // Validate billing address data.
        Billing::validate($data);

        // Assign address information.
        Billing::assignData($address, $customer, $data);

        // Resolve existing address from database.
        Billing::resolveByHash($address, $addressCollection);

        // Create address if it doesn't already exist.
        if (!Billing::exists($address)) {
            Billing::save($address);
        }

        /*
         * At this point the address must have either been loaded from the database, or created. Otherwise Prestashop
         * will allow the order to be created without any address data attached. So we have to double check that
         * everything went fine.
         */
        if (!Billing::exists($address)) {
            throw new Exception('Failed to resolve billing address.');
        }

        return $address;
    }

    /**
     * Sanitize data fields according to /presta-path/classes/Validate.php
     *
     * @param $data
     * @return mixed
     * @throws Exception
     */
    private static function getSanitizedNames($data)
    {
        $return = $data;

        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $fields = ['firstname', 'lastname'];
            foreach ($fields as $fieldName) {
                if (isset($data[$fieldName])) {
                    $return[$fieldName] = self::getSanitizedString($data[$fieldName]);
                }
            }
        }

        return $return;
    }

    /**
     * @param $data
     * @return array|string|string[]|null
     * @throws Exception
     */
    public static function getSanitizedString($data)
    {
        $return = $data;

        // Sanitize only if configured. Sanitizer is based on current PrestaShop rule set isName.
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $validityPattern = Tools::cleanNonUnicodeSupport(
                '/[0-9!<>,;?=+()@#"°{}_$%:¤]/u'
            );
            $return = preg_replace($validityPattern, '', $data);
        }

        return $return;
    }

    /**
     * Retrieve shipping address attached to $customer. If no address was found, create one. If an address was found,
     * update it with the provided shipping information and save it.
     *
     * @param PrestaCustomer $customer
     * @param int $languageId
     * @return PrestaAddress
     * @throws Exception
     */
    private static function resolveShippingAddress(
        PrestaCustomer $customer,
        $languageId
    ) {
        /** @var array $data */
        $data = Shipping::getSessionData();

        // Error emulation.
        //throw new Exception('No shipping address data in session.');

        if (!is_array($data) || !count($data)) {
            throw new Exception('No shipping address data in session.', 800);
        }

        // Get address collection from customer.
        $addressCollection = Shipping::getPrestaAddressCollection(
            $customer,
            (int)$languageId
        );

        /** @var PrestaAddress $address */
        $address = new PrestaAddress();

        // Ignore this section completely if not explicitly required.
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            // To sanitize initial information from $data, we for some reason have to sanitize the data first,
            // before saving the data back to the $data object if we don't want unrelated exceptions.
            $sanitizedNameArray = self::getSanitizedNames($data);
            $data = $sanitizedNameArray;
        }

        // Validate shipping address data.
        Shipping::validate($data);

        // Assign address information.
        Shipping::assignData($address, $customer, $data);

        // Resolve existing address by hash.
        Shipping::resolveByHash($address, $addressCollection);

        // Create address if it doesn't already exist.
        if (!Shipping::exists($address)) {
            Shipping::save($address);
        }

        /*
         * At this point the address must have either been loaded from the database, or created. Otherwise Prestashop
         * will allow the order to be created without any address data attached. So we have to double check that
         * everything went fine.
         */
        if (!Shipping::exists($address)) {
            throw new Exception('Failed to resolve shipping address.', 801);
        }

        return $address;
    }

    /**
     * Check that the provided argument value is a customer with an id. Please note that we cannot do an instanceof
     * check against "PrestaCustomer".
     *
     * @param mixed $customer
     * @return bool
     */
    private static function validatePrestaCustomer($customer)
    {
        return (
            ($customer instanceof PrestaCustomer) &&
            isset($customer->id) &&
            (int)$customer->id !== 0
        );
    }

    /**
     * Sync customer and address data to cart object before save.
     *
     * @param PrestaCart $cart
     * @param PrestaCustomer $customer
     * @param PrestaAddress $billingAddress
     * @param PrestaAddress|null $deliveryAddress
     * @param ResursBankCheckout $module
     * @return PrestaCart
     * @throws PrestaShopException
     */
    private static function prepareForOrderPlacement(
        PrestaCart $cart,
        PrestaCustomer $customer,
        PrestaAddress $billingAddress,
        PrestaAddress $deliveryAddress = null,
        ResursBankCheckout $module
    ) {
        // Store old shipping address id. We will need it towards the end of the method.
        $oldShippingAddressId = (int)$cart->id_address_delivery;

        // Overwrite customer id, secure key and billing address id.
        $cart->id_customer = $customer->id;
        $cart->secure_key = $customer->secure_key;
        $cart->id_address_invoice = $billingAddress->id;

        // Customizations checker: If there are customizations something bugs out on order placements
        // where the order suddenly gets id_address_delivery=0. By checking if there are customizations
        // we choose to reuse the old id until we find out whats really wrong.
        $hasCustomized = self::hasCustomizedProducts($cart);
        $allowCustomized = false;

        if ($hasCustomized && !$allowCustomized) {
            throw new PrestaShopException(
                $module->l('Cart contains unsupported products.'),
                999
            );
        }

        if (!$cart->id_address_delivery) {
            // If both of those two variants below comes out with a 0 in the delivery
            // address id, we're kind of screwed and the order will become "problematic" in
            // the address sections.
            if ($deliveryAddress !== null && isset($deliveryAddress->id) && (int)$deliveryAddress->id) {
                // Overwrite shopping address id (using billing address if no specific shipping address was provided).
                $cart->id_address_delivery = $deliveryAddress->id;
            } elseif ((int)$oldShippingAddressId) {
                $cart->id_address_delivery = $oldShippingAddressId;
            } else {
                // Usually for guests. Disabled. For now.
                //$cart->id_address_delivery = $billingAddress->id;
            }
        }

        /*$changedShipping = false;
        if (Session::has('resursbank_changed_shipping')) {
            $changedShipping = \ResursBankCheckout\Models\Service\Session::get('resursbank_changed_shipping');
        }*/

        if ($customer->isLogged()) {
            // Update delivery address on cart.
            if (isset($deliveryAddress->id) &&
                (int)$deliveryAddress->id > 0 &&
                $cart->id_address_delivery !== $deliveryAddress->id
            ) {
                $cart->id_address_delivery = $deliveryAddress->id;
            } else {
                // If the deliveryAddress is not set, we want to change the cart address to
                // the billing address as delivery address.
                $oldShippingAddressId = $billingAddress->id;
                $cart->id_address_delivery = $billingAddress->id;
                $cart->update(true);
                Session::set('resursbank_checkout_delivery_id', $billingAddress->id);
            }

            // Update billing address on cart.
            if (isset($billingAddress->id) &&
                $billingAddress->id > 0 &&
                $cart->id_address_invoice !== $billingAddress->id
            ) {
                $cart->id_address_invoice = $billingAddress->id;
            }
        }

        // Update the shipping address on all cart items (otherwise the order shipping address will be wrong).
        // For custom products, this is for some reason always 0.
        if ($oldShippingAddressId > 0 &&
            $cart->id_address_delivery > 0 &&
            $oldShippingAddressId != $cart->id_address_delivery
        ) {
            $cart->updateAddressId(
                $oldShippingAddressId,
                $cart->id_address_delivery
            );
        }

        // Always save last.
        $cart->save();

        return $cart;
    }

    /**
     * @param PrestaCart $cart
     * @return bool
     */
    private static function hasCustomizedProducts(PrestaCart $cart)
    {
        $return = false;
        $products = $cart->getProducts();

        foreach ($products as $product) {
            $customization = $cart->getProductCustomization($product['id_product']);
            if (is_array($customization) && count($customization)) {
                $return = true;
                break;
            }
        }

        return $return;
    }

    /**
     * Rebuild cart.
     *
     * @param ResursBankCheckout $module
     * @param PrestaCart $cart
     * @return bool
     * @throws Exception
     */
    public static function rebuild(ResursBankCheckout $module, PrestaCart $cart)
    {
        /** @var PrestaCart $newCart */
        $newCart = self::duplicateCart($cart);

        // Update the context to include the newly created cart, replacing any existing one.
        $module->getContext()->cookie->id_cart = $newCart->id;
        $module->getContext()->cart = $newCart;
        CartRule::autoAddToCart($module->getContext());
        $module->getContext()->cookie->write();

        // For safety sake, clear the API session keys.
        $module->getApi()->clearSession();

        return true;
    }

    /**
     * Duplicate PrestaCart object.
     *
     * @param PrestaCart $cart
     * @return PrestaCart
     * @throws Exception
     */
    private static function duplicateCart(PrestaCart $cart)
    {
        /** @var mixed $duplication */
        $duplication = $cart->duplicate();

        if (
            !is_array($duplication) ||
            !isset($duplication['success']) ||
            !((bool)$duplication['success']) ||
            !isset($duplication['cart']) ||
            !self::validateCartObject($duplication['cart'])
        ) {
            throw new Exception('Failed to duplicate cart.');
        }

        return $duplication['cart'];
    }

    /**
     * Validate that provided argument is an instance of PrestaCart.
     *
     * @param mixed $cart
     * @return bool
     */
    public static function validateCartObject($cart)
    {
        return (
            ($cart instanceof PrestaCart) &&
            isset($cart->id) &&
            (int)$cart->id !== 0
        );
    }

    /**
     * Generate cart validation key. This value will be included in the backUrl to identify cart ownership if the
     * payment fails, so the cart can safely be rebuilt and the previously created order cancelled.
     *
     * @param PrestaCart $cart
     * @return string
     */
    public static function getValidationKey(PrestaCart $cart)
    {
        /** @var PrestaCart $cart */
        return sha1(
            $cart->date_add .
            Configuration::get(Config::SETTING_SALT)
        );
    }

    /**
     * Clear session data.
     *
     * @param ResursBankCheckout $module
     */
    private static function clearSessionData(ResursBankCheckout $module)
    {
        // Clear customer data.
        Customer::clearSessionData();

        // Clear address data.
        Shipping::clearSessionData();
        Billing::clearSessionData();

        // Clear payment method data.
        Payment::clearSessionData();

        // Clear API data.
        $module->getApi()->clearSession();
    }
}
