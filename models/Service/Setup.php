<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use Configuration;
use Exception;
use Language;
use ResursBankCheckout;
use ResursBankCheckout\Models\Service\Order\State;
use Tab;

/**
 * Class Setup
 *
 * Setup service class. Contains methods to handle install/uninstall operations.
 *
 * @package ResursBankCheckout\Models\Service
 */
abstract class Setup
{
    /**
     * Installation procedure.
     *
     * @param ResursBankCheckout $module
     * @return void
     * @throws Exception
     */
    public static function install(ResursBankCheckout $module)
    {
        // Register an invisible dummy tab to get admin controllers working.
        self::registerDummyTab($module);

        // Install custom order states (pending, processing, completed).
        State::install($module);

        // Override default config values.
        self::overrideConfigValues();

        // Register custom hooks.
        self::registerHooks($module);
    }

    /**
     * Create a dummy tab in order to make admin controllers work. Since we do not actually need a tab we set its parent
     * to -1, so it simply won't show up. This is only done so that our admin controller will work and registercallbacks
     * can only be reached from the admin panel.
     *
     * @param ResursBankCheckout $module
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    private static function registerDummyTab(ResursBankCheckout $module)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminResursbankcheckout';
        $tab->name = [];

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'Resurs Bank Checkout';
        }

        $tab->id_parent = -1;
        $tab->module = $module->getName();
        $tab->add();
    }

    /**
     * Override some default config options in order for our module to function properly.
     */
    private static function overrideConfigValues()
    {
        // Settings which we will be overriding.
        $values = [
            'PS_ORDER_PROCESS_TYPE' => true,
            'PS_GUEST_CHECKOUT_ENABLED' => true,
            'PS_CONDITIONS' => false,
        ];

        // Previous setting values which we will backup.
        $defaultValues = [];

        foreach ($values as $key => $val) {
            // Store the current value this setting has.
            $defaultValues[$key] = Configuration::get($key);

            // Update the value to what we want it to be for the checkout to function properly.
            Configuration::updateValue($key, $val);
        }

        // Store the previous values in the database so we can re-apply them if the module is uninstalled.
        Configuration::updateValue(
            Config::SETTING_OVERRIDE_CONFIG_DEFAULTS,
            json_encode($defaultValues)
        );
    }

    /**
     * Register hooks.
     *
     * @param ResursBankCheckout $module
     * @throws Exception
     */
    private static function registerHooks(ResursBankCheckout $module)
    {
        foreach (self::getHooks() as $hook) {
            if (!$module->registerHook($hook)) {
                throw new Exception('Failed to register hook ' . $hook);
            }
        }
    }

    /**
     * Retrieve list of hooks to register.
     *
     * @return array
     */
    private static function getHooks()
    {
        return [
            'actionCartSave',
            'actionFrontControllerSetMedia',
            'actionDispatcherAfter',
            'backOfficeHeader',
            'displayBackOfficeTop',
            'footer',
            'header',
            'actionAjaxDieCartControllerdisplayAjaxUpdateBefore',
            'DisplayProductPriceBlock',
            'UpdateResursSsn',
        ];
    }

    /**
     * Uninstall module.
     *
     * @param ResursBankCheckout $module
     * @return void
     * @throws Exception
     */
    public static function uninstall(ResursBankCheckout $module)
    {
        $config = new \ResursBankCheckout\Models\Service\Config(null);

        // Delete stored callback SALT.
        Configuration::deleteByName(Config::SETTING_SALT);

        // Delete all configurable values.
        $config->deleteAllFormValues();

        // Reapply config values we override during installation.
        self::reapplyOverriddenConfigValues();

        // Delete backup setting.
        Configuration::deleteByName(Config::SETTING_OVERRIDE_CONFIG_DEFAULTS);
    }

    /**
     * Reapply previous config values.
     *
     * @throws Exception
     */
    private static function reapplyOverriddenConfigValues()
    {
        // Retrieve backup data.
        $data = Configuration::get(Config::SETTING_OVERRIDE_CONFIG_DEFAULTS);

        if (is_string($data) && $data !== '') {
            // Decode backup data to an array.
            $data = @json_decode($data, true);

            if (!is_array($data) || count($data) === 0) {
                throw new Exception('Failed to reapply overridden config values, corrupted backup data.');
            }

            // Reapply previous config values.
            foreach ($data as $key => $val) {
                $current = Configuration::get($key);

                if ($current !== false && is_string($current)) {
                    Configuration::updateValue($key, $val);
                }
            }
        }
    }
}
