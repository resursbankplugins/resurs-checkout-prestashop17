<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Customer;

use Configuration;
use ResursBankCheckout\Models\Service\Cart;
use ResursBankCheckout\Models\Service\Config;
use \Validate as PrestaValidate;
use \Exception;

/**
 * Class Validate
 *
 * Customer validation service class. Validates customer data submitted to us by the frontend iframe. Even though
 * Presta Shop performs its own validation it's necessary for us to validate the data forehand to ensure no malicious
 * data is stored within our session, where we keep all information until order placement occurs.
 *
 * @package ResursBankCheckout\Models\Service\Customer
 */
abstract class Validate
{
    /**
     * Check that all required $data properties exist on $this and that their have acceptable values.
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public static function validate(array $data)
    {
        self::haveRequiredProperties($data);

        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $data['firstname'] = Cart::getSanitizedString($data['firstname']);
            $data['lastname'] = Cart::getSanitizedString($data['lastname']);
        }

        self::firstName($data['firstname']);
        self::lastName($data['lastname']);
        self::email($data['email']);

        return true;
    }

    /**
     * Check that all required $data properties exist on $this.
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    private static function haveRequiredProperties(array $data)
    {
        foreach (self::getRequiredProperties() as $property) {
            if (
                !isset($data[$property]) ||
                $data[$property] === null ||
                $data[$property] === ''
            ) {
                throw new Exception('Missing customer property ' . $property);
            }
        }

        return true;
    }

    /**
     * Retrieve the required properties to have a valid customer.
     *
     * @return array
     */
    public static function getRequiredProperties()
    {
        return array(
            'firstname',
            'lastname',
            'email'
        );
    }

    /**
     * Validate property "firstname".
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function firstName($value)
    {
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $value = Cart::getSanitizedString($value);
        }

        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isName($value)
        ) {
            throw new Exception('Invalid customer firstname.');
        }

        return true;
    }

    /**
     * Validate property "lastname".
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function lastName($value)
    {
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $value = Cart::getSanitizedString($value);
        }

        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isName($value)
        ) {
            throw new Exception('Invalid customer lastname.');
        }

        return true;
    }

    /**
     * Validate email.
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function email($value)
    {
        if (!(bool) PrestaValidate::isEmail((string) $value)) {
            throw new Exception('Invalid customer email.');
        }

        return true;
    }
}
