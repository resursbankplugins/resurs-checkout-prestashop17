<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Customer\Address;

use Address as PrestaAddress;
use Customer as PrestaCustomer;
use Exception;
use Hook;
use Order as PrestaOrder;
use ResursBankCheckout;
use ResursBankCheckout\Models\Log;
use ResursBankCheckout\Models\Service\Cart;
use ResursBankCheckout\Models\Service\Config;
use ResursBankCheckout\Models\Service\Customer\Address;
use ResursBankCheckout\Models\Service\Session;
use stdClass;

/**
 * Class Billing
 *
 * Customer billing address service class. Extends the parent Address class to make use of its common methods. Contains
 * billing address specific methods/information.
 *
 * @package ResursBankCheckout\Models\Service\Customer\Address
 */
abstract class Billing extends Address
{
    /**
     * Session key where billing address data is stored.
     */
    const SESSION_DATA_KEY = 'resursbank_checkout_data_billing_address';

    /**
     * Session key where we store a flag indicating whether or not the billing address will also be used for shipping.
     */
    const SESSION_USE_FOR_SHIPPING_KEY = 'resursbank_checkout_data_billing_address_use_for_shipping';

    /**
     * Billing address alias.
     */
    const ALIAS = 'Folkbokföringsadress';

    /**
     * @var Log
     */
    private static $logHandler;

    /**
     * Handle billing data request. When a user submits billing data to us (normally through the controller
     * "savebilling") we use this method to handle that request, validating and storing the submitted data in the user
     * session.
     *
     * @return bool
     * @throws Exception
     */
    public static function handleDataRequest()
    {
        // Retrieve data from HTTP request.
        $data = self::getRequestData();

        if (isset($data['use_for_shipping'])) {
            self::setUseForShipping((int)$data['use_for_shipping'] === 1);
        }

        // Clean request data.
        parent::cleanRequestData($data, self::getAllowedProperties($data));

        // Validate user input.
        self::validate($data);

        // Store data in session.
        self::setSessionData($data);

        return true;
    }

    /**
     * Retrieve billing address data from request.
     *
     * @return array
     * @throws Exception
     */
    public static function getRequestData()
    {
        $data = isset($_REQUEST['billing']) ? $_REQUEST['billing'] : null;

        if (!is_string($data)) {
            throw new Exception('Billing data is not a string.');
        }

        $data = @json_decode($data, true);

        if (!is_array($data)) {
            throw new Exception('Billing data is not an array.');
        }

        return $data;
    }

    /**
     * Flag that we intend to use billing address for shipping.
     *
     * @param bool $val
     */
    public static function setUseForShipping($val)
    {
        Session::set(self::SESSION_USE_FOR_SHIPPING_KEY, (bool)$val);
    }

    /**
     * Retrieve all allowed address properties.
     *
     * @param array $requestData
     * @return array
     */
    private static function getAllowedProperties(array $requestData)
    {
        $result = Validate::getRequiredProperties();

        if (isset($requestData['ssn'])) {
            $result[] = 'ssn';
        }

        return $result;
    }

    /**
     * Load billing data from session and apply on this instance.
     *
     * @param array $data
     */
    private static function setSessionData(array $data)
    {
        Session::set(self::SESSION_DATA_KEY, $data);
    }

    /**
     * Retrieve billing data from session.
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public static function getSessionData($key = '')
    {
        return parent::getSessionSubData(self::SESSION_DATA_KEY, $key);
    }

    /**
     * Clear session data.
     */
    public static function clearSessionData()
    {
        Session::uns(self::SESSION_DATA_KEY);
    }

    /**
     * Whether or not to use the billing address for shipping.
     *
     * @return bool
     */
    public static function useForShipping()
    {
        return (bool)Session::get(self::SESSION_USE_FOR_SHIPPING_KEY);
    }

    /**
     * Assign address data.
     *
     * @param PrestaAddress $address
     * @param PrestaCustomer $customer
     * @param array $data
     */
    public static function assignData(
        PrestaAddress &$address,
        PrestaCustomer $customer,
        array $data
    ) {
        parent::assignData($address, $customer, $data);

        $address->alias = self::ALIAS;
    }

    /**
     * Update SSN on billing address if it's missing. This performs an API call
     * to retrieve the order payment from Resurs Bank and collect the SSN from
     * there. The SSN will only be missing if it wasn't filled out to collect
     * customer address information during the checkout process.
     *
     * @param ResursBankCheckout $module
     * @param PrestaOrder $order
     * @throws Exception
     */
    public static function updateMissingData(
        ResursBankCheckout $module,
        PrestaOrder $order
    ) {
        $customerSsnInfo = null;
        if (Config::getStoreSsn()) {
            /** @var PrestaAddress $billingAddress */
            $billingAddress = new PrestaAddress($order->id_address_invoice);

            if (self::isMissingSsn($billingAddress)) {
                try {
                    $customerSsnInfo = $module->getApi()->getPaymentSsn(
                        $order->reference
                    );
                    $ssnFailure = false;
                } catch (Exception $e) {
                    if ($e->getCode() === 4004) {
                        $ssnFailure = true;
                    } else {
                        throw $e;
                    }
                }

                if (!$ssnFailure) {
                    $billingAddress->vat_number = $customerSsnInfo;

                    // First time hook execution.
                    Hook::exec(
                        'UpdateResursSsn',
                        [
                            'order' => $order,
                            'vat_number' => $billingAddress->vat_number,
                        ]
                    );

                    $billingAddress->update();
                } else {
                    $storeData = [
                        'id_customer',
                        'lastname',
                        'firstname',
                        'address1',
                        'city',
                        'postcode',
                        'city',
                        'phone',
                        'phone_mobile',
                    ];
                    $showData = [];
                    foreach ($storeData as $storeKey) {
                        if (isset($billingAddress->{$storeKey})) {
                            $showData[$storeKey] = $billingAddress->{$storeKey};
                        }
                    }

                    self::getLogHandler()->log(
                        sprintf(
                            "%s failed to update SSN from Resurs Bank since there was no SSN to update in the payment. Customer information follows:\n%s",
                            __FUNCTION__,
                            print_r($showData, true)
                        )
                    );
                }
            }
        }
        // This data collection is not doing much, except for that it should compare address data in the
        // current customer setup and make the data synchronized. Synchronization is however currently disabled.
        $fullAddressInfo = $module->getApi()->getEcomConnection()->getPayment(
            $order->reference
        );

        $newBillingId = 0;
        $newDeliveryId = 0;
        $hasInternalBilling = false;
        $hasInternalDelivery = false;

        $resursCustomerPhoneNumber = isset($fullAddressInfo->customer->phone) ? $fullAddressInfo->customer->phone : '';
        // Check if the current customer address list contains any address that can be matched with getPayment-data.
        $addressList = $order->getCustomer()->getSimpleAddresses($order->getCustomer()->id_lang);
        $matchingBillingId = self::getMatchingAddress(
            $addressList,
            $fullAddressInfo->customer->address,
            $order->id_address_invoice,
            $resursCustomerPhoneNumber
        );
        $matchingDeliveryId = self::getMatchingAddress(
            $addressList,
            $fullAddressInfo->deliveryAddress,
            $order->id_address_delivery,
            $resursCustomerPhoneNumber
        );

        if ($matchingBillingId !== null) {
            // If we have a matching address id, prepare to use it.
            $newBillingId = $matchingBillingId;
            $hasInternalBilling = true;
            self::getLogHandler()->log(
                sprintf('Resynch Address: MatchBillingId (not null): %s.', $matchingBillingId)
            );
        }
        if ($matchingDeliveryId !== null) {
            // If we have a matching address id, prepare to use it.
            $newDeliveryId = $matchingDeliveryId;
            $hasInternalDelivery = true;
            self::getLogHandler()->log(
                sprintf('Resynch Address: MatchDeliveryId (not null): %s.', $matchingDeliveryId)
            );
        }

        if (!$hasInternalBilling) {
            // If we still have no matching customer address, try to synchronize it by a raw update.
            $newBillingId = self::updateAddress(
                new PrestaAddress($order->id_address_invoice),
                $fullAddressInfo->customer->address
            );
            self::getLogHandler()->log(
                sprintf('hasInternalBilling false. New id: %s.', $newBillingId)
            );
        }

        if (!$hasInternalDelivery) {
            // If we still have no matching customer address, try to synchronize it by a raw update.
            $newDeliveryId = self::updateAddress(
                new PrestaAddress($order->id_address_delivery),
                $fullAddressInfo->deliveryAddress
            );
            self::getLogHandler()->log(
                sprintf('hasInternalDelivery false. New id: %s.', $newDeliveryId)
            );
        }

        $updateOrder = false;
        if (null !== $newBillingId) {
            $order->id_address_invoice = $newBillingId;
            $updateOrder = true;
            self::getLogHandler()->log(
                'Update Order (Billing) Request set.'
            );
        }
        if (null !== $newDeliveryId) {
            $order->id_address_delivery = $newDeliveryId;
            $updateOrder = true;
            self::getLogHandler()->log(
                'Update Order (Delivery) Request set.'
            );
        }
        if ($updateOrder) {
            $order->save();
            self::getLogHandler()->log(
                '$order->save().'
            );
        }
    }

    /**
     * Check if provided address instance is missing a vat_number value.
     *
     * @param PrestaAddress $address
     * @return bool
     */
    public static function isMissingSsn(PrestaAddress $address)
    {
        return isset($address->vat_number) && $address->vat_number === '';
    }

    /**
     * Match customer address list with the address received in a getPayment block.
     * @param $addressListArray
     * @param $currentResursCustomerAddress
     * @param $actualId
     * @param $resursCustomerPhoneNumber
     * @return int|string|null
     */
    private static function getMatchingAddress(
        $addressListArray,
        $currentResursCustomerAddress,
        $actualId,
        $resursCustomerPhoneNumber
    ) {
        $matchData = [
            'firstname' => 'firstName',
            'lastname' => 'lastName',
            'address1' => 'addressRow1',
            'address2' => 'addressRow2',
            'postcode' => 'postalCode',
            'city' => 'postalArea',
        ];

        $currentAddressId = null;
        $hasActualId = null;
        foreach ($addressListArray as $addressId => $addressItem) {
            $isSameAddress = 0;
            if (self::isResursPhone($resursCustomerPhoneNumber, $addressItem)) {
                $isSameAddress++;
            }
            foreach ($matchData as $prestaItem => $matchItem) {
                if (isset($currentResursCustomerAddress->{$matchItem}) &&
                    $addressItem[$prestaItem] === $currentResursCustomerAddress->{$matchItem}
                ) {
                    $isSameAddress++;
                }
            }
            // Expecting the same address to have at least 5 exact matches (since the last row that *could* mismatch
            // is the one for addressRow2). If we have this kind of match, we are considered good to go with it.
            if ($isSameAddress >= 6) {
                $currentAddressId = $addressId;
                if ($currentAddressId === $actualId) {
                    $hasActualId = $addressId;
                }
            }
        }

        if ($hasActualId !== null && $currentAddressId > 0) {
            $currentAddressId = $hasActualId;
        }

        return $currentAddressId;
    }

    /**
     * Match customer phone number with phone number from Resurs Bank, simple mode.
     * @param $resursCustomerPhoneNumber
     * @param $prestaCustomer
     * @return bool
     */
    private static function isResursPhone($resursCustomerPhoneNumber, $prestaCustomer)
    {
        $return = false;
        $phoneCheck = ['phone', 'phone_mobile'];
        foreach ($phoneCheck as $phoneType) {
            if (isset($prestaCustomer[$phoneType])) {
                if (self::isMatchingPhoneNumbers($resursCustomerPhoneNumber, $prestaCustomer[$phoneType])) {
                    $return = true;
                    break;
                }
            }
        }
        return $return;
    }

    /**
     * Match two phone numbers with each other where Resurs Bank phone number is stripped from country code.
     *
     * @param $resursCustomerPhoneNumber
     * @param $prestaCustomerPhone
     * @return bool
     */
    private static function isMatchingPhoneNumbers($resursCustomerPhoneNumber, $prestaCustomerPhone)
    {
        $countryCodeFixedPhoneNumber = preg_replace('/^0/', '', $prestaCustomerPhone);
        return (bool)preg_match(sprintf('/%s/', $countryCodeFixedPhoneNumber), $resursCustomerPhoneNumber);
    }

    /**
     * Synchronize address data in PrestaShop with Resurs Bank getAddress().
     * @param PrestaAddress $address
     * @param array|stdClass $resursPayment
     * @return int
     * @throws Exception
     */
    private static function updateAddress(PrestaAddress $address, $resursPayment)
    {
        $saveAddress = false;
        $transitionData = [
            'firstname' => 'firstName',
            'lastname' => 'lastName',
            'address1' => 'addressRow1',
            'address2' => 'addressRow2',
            'postcode' => 'postalCode',
            'city' => 'postalArea',
        ];

        $sanitizeData = [
            'firstname', 'lastname'
        ];

        $addressRequestType = null;
        foreach ($transitionData as $fromKey => $toKey) {
            if (isset($resursPayment->{$toKey})) {
                if ($address->{$fromKey} !== $resursPayment->{$toKey}) {
                    $address->{$fromKey} = !in_array($fromKey, $sanitizeData) ? $resursPayment->{$toKey} : Cart::getSanitizedString($resursPayment->{$toKey});
                    $addressRequestType = 'add';
                }
                self::getLogHandler()->log($resursPayment->{$toKey});
            }
        }

        switch ($addressRequestType) {
            case 'add':
                $saveAddress = $address->add();
                break;
            default:
        }
        self::getLogHandler()->log(
            sprintf(
                'Address update status: %s. Address synchronization set to %s.',
                (isset($saveAddress) && $saveAddress ? 'Changed.' : 'Unchanged.'),
                $addressRequestType
            )
        );

        return $address->id;
    }

    /**
     * Retrieve log handler.
     *
     * @return Log
     */
    private static function getLogHandler()
    {
        if (self::$logHandler === null) {
            self::$logHandler = new Log();
        }

        return self::$logHandler;
    }

    /**
     * Create log entry.
     *
     * @param string|Exception $message
     * @param array $context
     * @return ResursBankCheckout
     * @throws Exception
     */
    public static function log($message, array $context = [])
    {
        self::getLogHandler()->log($message, $context);
    }
}
