<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Customer\Address;

use Configuration;
use ResursBankCheckout\Models\Service\Cart;
use ResursBankCheckout\Models\Service\Config;
use \Validate as PrestaValidate;
use \Exception;

/**
 * Class Validate
 *
 * Customer address validation service class. Validates address data submitted to us by the frontend iframe. Even though
 * Presta Shop performs its own validation it's necessary for us to validate the data forehand to ensure no malicious
 * data is stored within our session, where we keep all information until order placement occurs.
 *
 * @package ResursBankCheckout\Models\Service\Customer
 */
abstract class Validate
{
    /**
     * Check that all required $data properties exist on $this and that their have acceptable values.
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public static function validate(array $data)
    {
        self::haveRequiredProperties($data);

        self::firstName($data['firstname']);
        self::lastName($data['lastname']);
        self::countryId($data['country_id']);
        self::street($data['street']);
        self::postCode($data['postcode']);
        self::city($data['city']);
        self::telephone($data['telephone']);

        if (
            isset($data['ssn']) &&
            (string) $data['ssn'] !== ''
        ) {
            self::ssn($data['ssn']);
        }

        return true;
    }

    /**
     * Check that all required $data properties exist on $this.
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    private static function haveRequiredProperties(array $data)
    {
        foreach (self::getRequiredProperties() as $property) {
            if (
                !isset($data[$property]) ||
                $data[$property] === null ||
                $data[$property] === ''
            ) {
                throw new Exception('Missing address property ' . $property);
            }
        }

        return true;
    }

    /**
     * Retrieve the required properties to have a valid address.
     *
     * @return array
     */
    public static function getRequiredProperties()
    {
        return array(
            'firstname',
            'lastname',
            'country_id',
            'street',
            'postcode',
            'city',
            'telephone'
        );
    }

    /**
     * Validate property "firstname".
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function firstName($value)
    {
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $value = Cart::getSanitizedString($value);
        }

        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isName($value)
        ) {
            throw new Exception('Invalid address firstname.');
        }

        return true;
    }

    /**
     * Validate property "lastname".
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function lastName($value)
    {
        if ((bool)Configuration::get(Config::SETTING_SANITIZE_CUSTOMER_VALUES)) {
            $value = Cart::getSanitizedString($value);
        }

        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isName($value)
        ) {
            throw new Exception('Invalid address lastname.');
        }

        return true;
    }

    /**
     * Validate country ISO.
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function countryId($value)
    {
        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isLanguageIsoCode($value)
        ) {
            throw new Exception('Invalid country.');
        }

        return true;
    }

    /**
     * Validate street address.
     *
     * @param array $data (note that we intentionally avoid strict argument type since we simply forward user input).
     * @return bool
     * @throws Exception
     */
    private static function street($data)
    {
        if (!is_array($data)) {
            throw new Exception('Street needs to be formatted as an array.');
        }

        self::validatePrimaryStreet($data);
        self::validateSecondaryStreet($data);

        return true;
    }

    /**
     * Validate primary street address (first street field). This expects the $data property "street" on $this
     * instance to be an array!
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    private static function validatePrimaryStreet(array $data)
    {
        if (
            !isset($data[0]) ||
            !is_string($data[0]) ||
            !(bool) PrestaValidate::isAddress($data[0])
        ) {
            throw new Exception('Missing or invalid primary street.');
        }

        return true;
    }

    /**
     * Validate secondary street field (second (final) street field). This expects the $data property "street" on $this
     * instance to be an array!
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    private static function validateSecondaryStreet(array $data)
    {
        if (isset($data[1])) {
            if (
                !is_string($data[1]) ||
                !(bool) PrestaValidate::isAddress($data[1])
            ) {
                throw new Exception('Invalid secondary street.');
            }
        }

        return true;
    }

    /**
     * Validate post code.
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function postCode($value)
    {
        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isPostCode($value)
        ) {
            throw new Exception('Invalid post code.');
        }

        return true;
    }

    /**
     * Validate city.
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function city($value)
    {
        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isCityName($value)
        ) {
            throw new Exception('Invalid city.');
        }

        return true;
    }

    /**
     * Validate telephone.
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function telephone($value)
    {
        if (
            !is_string($value) ||
            !(bool) PrestaValidate::isPhoneNumber($value)
        ) {
            throw new Exception('Invalid telephone number.');
        }

        return true;
    }

    /**
     * Validate SSN.
     *
     * @param string $value
     * @return bool
     * @throws Exception
     */
    private static function ssn($value)
    {
        if (!(bool) preg_match('/^[0-9\-]{10,13}/', (string) $value)) {
            throw new Exception('Invalid SSN.');
        }

        return true;
    }
}
