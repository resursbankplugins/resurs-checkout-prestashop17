<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service\Customer;

use \Tools;
use \Exception;
use \Address as PrestaAddress;
use \Customer as PrestaCustomer;
use \ResursBankCheckout\Models\Service\Customer\Address\Validate;
use \ResursBankCheckout\Models\Service\Session;

/**
 * Class Address
 *
 * Customer address service class. Contains common methods to handle customer address operations. There are separate
 * subclasses containing specific methods/information for shipping/billing address.
 *
 * @package ResursBankCheckout\Models\Service\Customer
 */
abstract class Address
{
    /**
     * Resolve and overwrite provided address with existing one in database if it exist.
     *
     * @param PrestaAddress $address
     * @param array $collection
     */
    public static function resolveByHash(PrestaAddress &$address, array $collection)
    {
        if (count($collection)) {
            $hash = self::getHash($address);

            foreach ($collection as $existingAddress) {
                $existingAddress = new PrestaAddress($existingAddress['id_address']);

                if (self::getHash($existingAddress) === $hash) {
                    $address = $existingAddress;
                    break;
                }
            }
        }
    }

    /**
     * Retrieve address hash.
     *
     * @param PrestaAddress $address
     * @return string
     */
    public static function getHash(PrestaAddress $address)
    {
        return sha1(preg_replace('/[^a-z0-9]/isU', '', strtolower(
            $address->id_customer .
            $address->id_country .
            $address->firstname .
            $address->lastname .
            $address->address1 .
            $address->address2 .
            $address->postcode .
            $address->city .
            $address->phone .
            $address->phone_mobile .
            $address->alias
        )));
    }

    /**
     * Retrieve all addresses associated with a customer.
     *
     * @param PrestaCustomer $customer
     * @param int $langId
     * @return array
     */
    public static function getPrestaAddressCollection(PrestaCustomer $customer, $langId)
    {
        return $customer->getAddresses($langId);
    }

    /**
     * Update or create Prestashop address based on the data on this instance.
     *
     * @param PrestaAddress $address
     */
    public static function save(PrestaAddress &$address)
    {
        if (self::exists($address)) {
            $address->update();
        } else {
            $address->add();
        }
    }

    /**
     * Check if the address exists.
     *
     * @param PrestaAddress $address
     * @return bool
     */
    public static function exists(PrestaAddress $address)
    {
        return (isset($address->id) && (int) $address->id !== 0);
    }

    /**
     * Assign data to address object.
     *
     * @param PrestaAddress $address
     * @param PrestaCustomer $customer
     * @param array $data
     */
    public static function assignData(
        PrestaAddress &$address,
        PrestaCustomer $customer,
        array $data
    ) {
        $address->id_customer = (int) $customer->id;
        $address->id_country = (int) Tools::getCountry($address);
        $address->country = (string)  $data['country_id'];
        $address->firstname = (string) $data['firstname'];
        $address->lastname = (string) $data['lastname'];
        $address->address1 = self::getStreet(0, $data);
        $address->address2 = self::getStreet(1, $data);
        $address->postcode = (string) $data['postcode'];
        $address->city = (string) $data['city'];
        $address->phone = (string) $data['telephone'];
        $address->phone_mobile = (string) $data['telephone'];

        if (isset($data['ssn'])) {
            $address->vat_number = $data['ssn'];
        }
    }

    /**
     * Retrieve street line.
     *
     * @param int $line
     * @param array $data
     * @return string
     */
    private static function getStreet($line, array $data)
    {
        $result = '';

        $line = (int) $line;

        if (
            isset($data['street']) &&
            is_array($data['street']) &&
            isset($data['street'][$line])
        ) {
            $result = (string) $data['street'][$line];
        }

        return $result;
    }

    /**
     * Short-hand to validation method.
     *
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public static function validate(array $data)
    {
        return Validate::validate($data);
    }

    /**
     * Retrieve address data from session.
     *
     * @param string $sessionKey
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    protected static function getSessionSubData($sessionKey, $key = '')
    {
        $result = Session::get($sessionKey);

        if (!is_array($result)) {
            $result = array();
        }

        if (is_string($key) && $key !== '') {
            if (!isset($result[$key]) && $key !== 'ssn') {
                throw new Exception('Missing address property ' . $key);
            }

            // Fields that exists does not have any problems to show the result, however
            // if someone wrongfully asks for - specifically - ssn even if it does not exist, this will generate
            // further notices about missing index. Also, as we don't know how the requested return data actually
            // looks or are typed, we're very much forced to set at least an empty value.
            $result = isset($result[$key]) ? $result[$key] : '';
        }

        return $result;
    }

    /**
     * Clean up request data object.
     *
     * @param array $data
     * @param array $allowedProperties
     */
    protected static function cleanRequestData(
        array &$data,
        array $allowedProperties
    ) {
        foreach ($data as $key => $val) {
            if (!in_array($key, $allowedProperties)) {
                unset($data[$key]);
            }
        }
    }
}
