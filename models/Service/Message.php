<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use \ResursBankCheckout\Models\Service\Session;

/**
 * Class Message
 *
 * Contains methods to handle message bags.
 *
 * @package ResursBankCheckout\Models\Service
 */
class Message
{
    /**
     * Message type for errors.
     */
    const TYPE_ERROR = 'error';

    /**
     * Name of general message bag.
     *
     * @var string
     */
    protected static $bagName = 'general';

    /**
     * Retrieve messages from specified bag.
     *
     * @param string $type
     * @param bool $flush
     * @return array
     */
    public static function get($type, $flush = true)
    {
        $messages = Session::get(self::getBagName($type));

        if ($flush) {
            self::flush($type);
        }

        return is_array($messages) ? $messages : array();
    }

    /**
     * Add message to specified bag.
     *
     * @param string $type
     * @param string $message
     */
    public static function add($type, $message)
    {
        $messages = self::get($type);
        $messages[] = (string) $message;

        Session::set(self::getBagName($type), $messages);
    }

    /**
     * Delete messages from specified bag.
     *
     * @param string $type
     */
    public static function flush($type)
    {
        Session::uns(self::getBagName($type));
    }

    /**
     * Retrieve bag name.
     *
     * @param string $type
     * @return string
     */
    protected static function getBagName($type)
    {
        return 'message_' . (string) self::$bagName . '_' . (string) $type;
    }
}
