<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Service;

use \Exception;
use \Configuration;
use \OrderHistory;
use \Cart as PrestaCart;
use \Order as PrestaOrder;
use \Customer as PrestaCustomer;
use \Address as PrestaAddress;
use \ResursBankCheckout;
use \ResursBankCheckout\Models\Service\Order\State;

/**
 * Class Order
 *
 * Order service class. Contains methods to handle orders.
 *
 * @package ResursBankCheckout\Models\Service
 */
abstract class Order
{
    /**
     * Load order from cart object.
     *
     * @param PrestaCart $cart
     * @return PrestaOrder
     * @throws Exception
     */
    public static function getByCart(PrestaCart $cart)
    {
        $orderId = (int) PrestaOrder::getOrderByCartId($cart->id);

        if ($orderId === 0) {
            throw new Exception('Failed to find order associated with cart ' . $cart->id, 804);
        }

        $result = new PrestaOrder($orderId, $cart->id_lang);

        if (!$result->id) {
            throw new Exception('Failed to load order with id ' . $orderId, 805);
        }

        return $result;
    }

    /**
     * Create a new order.
     *
     * @param ResursBankCheckout $module
     * @param PrestaCart $cart
     * @param PrestaCustomer $customer
     * @param PrestaAddress $billingAddress
     * @param string $paymentMethod
     * @return bool
     * @throws Exception
     */
    public static function create(
        ResursBankCheckout $module,
        PrestaCart $cart,
        PrestaCustomer $customer,
        PrestaAddress $billingAddress,
        $paymentMethod
    ) {
        try {
            /*if (!$customer->isLogged() && $cart->id_address_delivery) {
                throw new Exception($module->l('It seems that you are already a customer in our system. Try to login instead, to continue.'), 999);
            }*/

            $orderValidation = (bool)$module->validateOrder(
                $cart->id,
                (int)Configuration::get(State::PENDING),
                $cart->getOrderTotal(),
                (string)$paymentMethod,
                null,
                array(),
                (int)$module->getContext()->currency->id,
                false,
                $customer->secure_key
            );
        } catch (Exception $orderValidationException) {
            throw $orderValidationException;
        }

        return $orderValidation;
    }

    /**
     * Cancel order.
     *
     * @param PrestaOrder $order
     * @return bool
     * @throws Exception
     */
    public static function cancel(PrestaOrder $order)
    {
        self::changeOrderState($order, _PS_OS_CANCELED_);
        $order->update();

        return true;
    }

    /**
     * Change state of provided order.
     *
     * @param PrestaOrder $order
     * @param int $state
     * @throws Exception
     */
    public static function changeOrderState(PrestaOrder &$order, $state)
    {
        $state = (int) $state;

        if (!isset($order->id) || (int) $order->id === 0) {
            throw new Exception('Cannot change state for none existing order.');
        }

        if ($state > 0 && (int) $order->current_state !== $state) {
            /** @var OrderHistory $history */
            $history = new OrderHistory();

            $history->id_order = $order->id;
            $history->id_employee = 0;

            // Update order state.
            $history->changeIdOrderState($state, $order, !$order->hasInvoice());

            // Send email, if any. If this fails, the problem may be environment based.
            // Such problems leads to that the curstomer never gets any cancellation e-mail.
            // If we however throw an exception at this moment, problems with sending
            // back customers to their cart could occur.
            $history->addWithemail();

            // Set current order state on order object.
            $order->current_state = $state;
        }
    }

    /**
     * Check if the provided order has been paid for using Resurs Bank.
     *
     * @param PrestaOrder $order
     * @return bool
     */
    public static function isPaidWithResursBank(PrestaOrder $order)
    {
        return $order->module === ResursBankCheckout::NAME;
    }
}
