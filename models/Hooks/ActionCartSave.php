<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use \ResursBankCheckout;
use \Exception;
use \DispatcherCore;

/**
 * Class ActionCartSave
 *
 * Hook for event "ActionCartSave". This will update the payment information at Resurs Bank.
 *
 * @package ResursBankCheckout\Models\Hooks
 */
class ActionCartSave
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * ActionCartSave constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = array()
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Update payment at Resurs Bank to match current cart information.
     */
    public function execute()
    {
        try {
            if ($this->isActive()) {
                // Update payment session.
                $this->module->getApi()->updatePayment(
                    $this->module->getApi()->getPaymentId(),
                    $this->module->getApi()->getPaymentLinesFromCart(
                        $this->module->getContext()->cart
                    )
                );
            }
        } catch (Exception $e) {
            $this->module->log($e, $_REQUEST);
        }
    }

    /**
     * Check if this hook is activate.
     *
     * @return bool
     * @throws \PrestaShopException
     */
    public function isActive()
    {
        $currentController = DispatcherCore::getInstance()->getController();

        return (
            $this->module->enabled() &&
            ($currentController === "orderopc" ||
                $currentController === "cart" ||
                $currentController === "savedelivery") &&
            $this->module->cartExistsInContext() &&
            $this->module->getApi()->paymentSessionInitiated()
        );
    }
}
