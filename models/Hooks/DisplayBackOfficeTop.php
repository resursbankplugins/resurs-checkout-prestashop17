<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Hooks;

use Context;
use Exception;
use Order;
use ResursBankCheckout;
use ResursBankCheckout\Models\Service\Order as OrderService;
use Tools;

/**
 * Class DisplayBackOfficeTop
 *
 * Hook for the event "DisplayBackOfficeTop". This will append payment
 * information from Resurs Bank to the order view in the administration panel.
 *
 * @package ResursBankCheckout\Models\Hooks
 */
class DisplayBackOfficeTop
{
    /**
     * @var ResursBankCheckout
     */
    private $module;

    /**
     * Event parameters.
     *
     * @var array
     */
    private $params;

    /**
     * @var Order
     */
    private $order = null;

    /**
     * DisplayBackOfficeTop constructor.
     *
     * @param ResursBankCheckout $module
     * @param array $params
     */
    public function __construct(
        ResursBankCheckout $module,
        array $params = []
    ) {
        $this->module = $module;
        $this->params = $params;
    }

    /**
     * Display custom Resurs Bank payment information on order view.
     *
     * @return string
     */
    public function execute()
    {
        $result = '';

        try {
            if ($this->isActive()) {
                // Fetch payment information through ECom.
                $payment = $this->module->getApi()->ecom()->getPayment(
                    $this->getOrder()->reference
                );

                if (!isset($payment->status)) {
                    // Payment has no statuses yet, so we're initiating an empty array for it to prepare for moments
                    // where the payment is not yet fixed.
                    $payment->status = [];
                }

                // Status should be formatted as an array.
                if (!is_array($payment->status)) {
                    $payment->status = [$payment->status];
                } else {
                    $payment->status = [];
                }

                // Assign template variables.
                $this->module->getContext()->smarty->assign([
                    'payment' => $payment,
                    'logo' => $this->module->getPath('views/img/rb_white.png'),
                ]);

                // Render custom template containing Resurs Bank payment information.
                $result = $this->module->display(
                    $this->module->getPath('resursbankcheckout.php'),
                    'payment_information.tpl'
                );
            }
        } catch (Exception $e) {
            $this->module->log($e);

            $errorCode = (int)$e->getCode();
            if (in_array($errorCode, [3, 8, 404])) {
                $this->module->getContext()->smarty->assign([
                    'logo' => $this->module->getPath('views/img/rb_white.png'),
                ]);

                $result = $this->module->display(
                    $this->module->getPath('resursbankcheckout.php'),
                    'payment_information_failure.tpl'
                );
            }
        }

        return $result;
    }

    /**
     * Retrieve order from database based on request.
     *
     * @return Order
     * @throws Exception
     */
    public function getOrder()
    {
        if ($this->order === null) {
            // Retrieve order id from request.
            $id = (int)Tools::getValue('id_order');

            if ($id === 0) {
                throw new Exception('No order id.');
            }

            // Load order and store on this instance for later use.
            $this->order = new Order($id);

            if (
                !($this->order instanceof Order) ||
                (int)$this->order->id === 0
            ) {
                throw new Exception('Failed to load order with id ' . $id);
            }
        }

        return $this->order;
    }

    /**
     * Check whether or not this hook is active.
     *
     * @return bool
     * @throws Exception
     */
    public function isActive()
    {
        $controller = strtolower(Context::getContext()->controller->controller_name);

        return (
            $controller === 'adminorders' &&
            (int)Tools::getValue('id_order') !== 0 &&
            OrderService::isPaidWithResursBank($this->getOrder())
        );
    }
}
