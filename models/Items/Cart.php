<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Items;

use \Cart as PrestaShopCart;
use \CartRule;
use \Carrier;
use Configuration;
use Context;
use \Exception;
use \ResursBankCheckout\Models\Items;
use \ResursBankCheckout\Models\Service\Config;
use Tools;

/**
 * Class Cart
 *
 * Contains methods to collect payment lines from a shopping cart, including discount and shipping information.
 *
 * @package Resursbankcheckout\Models\Items
 */
class Cart extends Items
{
    /**
     * Retrieve all payment lines formatted for the API payload.
     *
     * @param PrestaShopCart $cart
     * @return array
     * @throws Exception
     */
    public function getPaymentLines(PrestaShopCart $cart)
    {
        return parent::compilePaymentLines(
            $this->getProductLines($cart),
            $this->getDiscountLines($cart),
            $this->getShippingLine($cart)
        );
    }

    /**
     * Retrieve product lines.
     *
     * @param PrestaShopCart $cart
     * @return array
     * @throws Exception
     */
    public function getProductLines(PrestaShopCart $cart)
    {
        $result = array();

        $products = $cart->getProducts();

        if (!count($products)) {
            throw new Exception($this->module->l('No products to retrieve payment lines from.'));
        }

        foreach ($products as $product) {
            if ($this->validateProductLine($product)) {
                $result[] = $this->getProductLine($product);
            }
        }

        return $result;
    }

    /**
     * Retrieve all discount lines.
     *
     * @param PrestaShopCart $cart
     * @return array
     */
    public function getDiscountLines(PrestaShopCart $cart)
    {
        $result = array();

        // Get all discount values applied to the shopping cart.
        $cartRules = $cart->getCartRules(CartRule::FILTER_ACTION_REDUCTION);

        if (count($cartRules) > 0) {
            // Handle each discount line separately.
            foreach ($cartRules as $rule) {
                if ($this->validateDiscountLine($rule)) {
                    // Retrieve formatted discount line.
                    $result[] = $this->getDiscountLine($rule);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve shipping fee line.
     *
     * @param PrestaShopCart $cart
     * @return array
     * @throws Exception
     */
    public function getShippingLine(PrestaShopCart $cart)
    {
        return parent::compileShippingLine(
            $cart->getOrderTotal(false, PrestaShopCart::ONLY_SHIPPING),
            $this->getShippingTax($cart),
            $this->getShippingLineDescription($cart)
        );
    }

    /**
     * Retrieve shipping line description (shipping carrier name/identifier). We also sanitize the name.
     *
     * @param PrestaShopCart $cart
     * @return string
     * @throws Exception
     */
    public function getShippingLineDescription(PrestaShopCart $cart)
    {
        $carrierId = (int) $cart->id_carrier;
        $carrier = new Carrier($cart->id_carrier, $cart->id_lang);
        $result = '';

        if ($carrierId !== 0) {
            if (!isset($carrier->name) || !isset($carrier->id_reference)) {
                throw new Exception('Invalid shipping carrier. Missing name/reference.');
            }

            $result = (string) preg_replace(
                '/[^a-z0-9\_]/isU',
                '',
                (
                    preg_replace('/\ /', '_', $carrier->name) .
                    '_' .
                    $carrier->id_reference
                )
            );
        }

        return $result;
    }

    /**
     * Retrieve discount payment line. This method assumes the $data argument has all required properties. Please
     * validate data using the validation method before passing it here.
     *
     * @param array $data
     * @return array
     */
    public function getDiscountLine(array $data)
    {
        return parent::compileDiscountLine(
            (string) $data['code'],
            (string) $data['name'],
            (float) $data['value_tax_exc'],
            parent::calculateDiscountTax(
                (float) $data['value_real'],
                (float) $data['value_tax_exc']
            )
        );
    }

    /**
     * Convert PrestaShopCart_Item to an order line for the API.
     *
     * @param array $product
     * @return array
     * @throws Exception
     */
    public function getProductLine(array $product)
    {
        $rate = isset($product['rate']) ? $product['rate'] : 0;

        return parent::compileProductLine(
            $product['reference'],
            $this->getProductName($product),
            (float) $product['quantity'],
            $this->getProductUnit($product),
            $this->getProductPrice($product),
            (float) $rate
        );
    }

    /**
     * Validate data we intend to use for a discount line in an API payload.
     *
     * @param array $data
     * @return bool
     */
    public function validateDiscountLine(array $data)
    {
        return (
            isset($data['code']) &&
            isset($data['name']) &&
            isset($data['value_real']) &&
            isset($data['value_tax_exc'])
        );
    }

    /**
     * Validate data we intend to use for a product line in an API payload.
     *
     * @param array $product
     * @return bool
     */
    public function validateProductLine(array $product)
    {
        return (
            isset($product['reference']) &&
            isset($product['name']) &&
            isset($product['quantity']) &&
            isset($product['rate']) &&
            (float) $product['quantity'] > 0
        );
    }

    /**
     * Retrieve payment line price from product array.
     *
     * @param array $product
     * @return float
     * @throws Exception
     */
    public function getProductPrice(array $product)
    {
        $result = 0.0;
        $properties = [];

        // All of these properties can contain a price we can use. Use the first we find defined on the product.
        $properties[] = 'price_with_reduction_without_tax';
        $properties[] = 'price';
        $properties[] = 'price_attribute';

        foreach ($properties as $property) {
            if (isset($product[$property])) {
                // If we can collect a price value above "0.0" we stick with it.
                //$result = round((float) $product[$property], 4);
                $result = Tools::ps_round(
                    (float) $product[$property],
                    Context::getContext()->getComputingPrecision()
                );

                if ($result > 0) {
                    break;
                }
            }
        }

        // If we couldn't collect a price from the product we cannot continue any further.
        if ($result === 0.0) {
            throw new Exception('Failed to collect price from product ' . json_encode($product));
        }

        return $result;
    }

    /**
     * Retrieve product unit.
     *
     * @param array $product
     * @return string
     */
    public function getProductUnit(array $product)
    {
        return (
            isset($product['unity']) &&
            is_string($product['unity']) &&
            $product['unity'] !== ''
        ) ? (string) $product['unity'] : 'st';
    }

    /**
     * Retrieve product payment line name.
     *
     * @param array $product
     * @return string
     */
    public function getProductName(array $product)
    {
        $result = $product['name'];

        if (
            isset($product['attributes_small']) &&
            $product['attributes_small'] !== ''
        ) {
            $result .=  ' (' . $product['attributes_small'] . ')';
        }

        return (string) $result;
    }

    /**
     * Retrieve shipping tax percentage.
     *
     * @param PrestaShopCart $cart
     * @return float|int
     */
    public function getShippingTax(PrestaShopCart $cart)
    {
        return $this->calculateShippingTax(
            $cart->getOrderTotal(true, PrestaShopCart::ONLY_SHIPPING),
            $cart->getOrderTotal(false, PrestaShopCart::ONLY_SHIPPING)
        );
    }
}
