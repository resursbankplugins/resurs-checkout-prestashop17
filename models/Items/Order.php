<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ResursBankCheckout\Models\Items;

use \Order as PrestaOrder;
use \Carrier;
use \CartRule;
use \Exception;
use \ResursBankCheckout\Models\Items;

/**
 * Class Order
 *
 * Contains methods to collect payment lines from an order, including discount and shipping information.
 *
 * @package Resursbankcheckout\Models\Items
 */
class Order extends Items
{
    /**
     * Retrieve all payment lines formatted for the API payload.
     *
     * @param PrestaOrder $order
     * @return array
     * @throws Exception
     */
    public function getPaymentLines(PrestaOrder $order)
    {
        return parent::compilePaymentLines(
            $this->getProductLines($order),
            $this->getDiscountLines($order),
            $this->getShippingLine($order)
        );
    }

    /**
     * Retrieve product lines.
     *
     * @param PrestaOrder $order
     * @return array
     * @throws Exception
     */
    public function getProductLines(PrestaOrder $order)
    {
        $result = array();

        $products = $order->getProducts();

        if (!count($products)) {
            throw new Exception($this->module->l('No products to retrieve payment lines from.'));
        }

        foreach ($products as $product) {
            if ($this->validateProductLine($product)) {
                $result[] = $this->getProductLine($product);
            }
        }

        return $result;
    }

    /**
     * Convert PrestaOrder_Item to an order line for the API.
     *
     * @param array $product
     * @return array
     * @throws Exception
     */
    public function getProductLine(array $product)
    {
        return parent::compileProductLine(
            $product['product_reference'],
            $product['product_name'],
            (float) $product['product_quantity'],
            $this->getProductUnit($product),
            (float) $product['unit_price_tax_excl'],
            $this->calculateProductTax(
                $product['unit_price_tax_incl'],
                $product['unit_price_tax_excl']
            )
        );
    }

    /**
     * Validate data we intend to use for a product line in an API payload.
     *
     * @param array $product
     * @return bool
     */
    public function validateProductLine(array $product)
    {
        return (
            isset($product['product_reference']) &&
            isset($product['product_name']) &&
            isset($product['product_quantity']) &&
            isset($product['unit_price_tax_excl']) &&
            isset($product['unit_price_tax_incl']) &&
            (float) $product['product_quantity'] > 0
        );
    }

    /**
     * Retrieve product unit.
     *
     * @param array $product
     * @return string
     */
    public function getProductUnit(array $product)
    {
        return (
            isset($product['unity']) &&
            is_string($product['unity']) &&
            $product['unity'] !== ''
        ) ? (string) $product['unity'] : 'st';
    }

    /**
     * Retrieve all discount lines.
     *
     * @param PrestaOrder $order
     * @return array
     * @throws Exception
     */
    public function getDiscountLines(PrestaOrder $order)
    {
        $result = array();

        // Get all discount values applied to the order.
        $discountCollection = $order->getDiscounts();

        if (count($discountCollection) > 0) {
            // Handle each discount line separately.
            foreach ($discountCollection as $discount) {
                if ($this->validateDiscountLine($discount)) {
                    // Retrieve formatted discount line.
                    $result[] = $this->getDiscountLine($order, $discount);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve discount payment line. This method assumes the $data argument has all required properties. Please
     * validate data using the validation method before passing it here.
     *
     * @param PrestaOrder $order
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getDiscountLine(PrestaOrder $order, array $data)
    {
        return parent::compileDiscountLine(
            $this->getDiscountCode($order, (int) $data['id_cart_rule']),
            (string) $data['name'],
            (float) $data['value_tax_excl'],
            parent::calculateDiscountTax(
                (float) $data['value'],
                (float) $data['value_tax_excl']
            )
        );
    }

    /**
     * Validate data we intend to use for a discount line in an API payload.
     *
     * @param array $data
     * @return bool
     */
    public function validateDiscountLine(array $data)
    {
        return (
            isset($data['id_cart_rule']) &&
            isset($data['name']) &&
            isset($data['value']) &&
            isset($data['value_tax_excl']) &&
            (int) $data['id_cart_rule'] > 0
        );
    }

    /**
     * Retrieve discount rule code.
     *
     * @param PrestaOrder $order
     * @param int $ruleId
     * @return string
     * @throws Exception
     */
    public function getDiscountCode(PrestaOrder $order, $ruleId)
    {
        $rule = new CartRule($ruleId, $order->id_lang, $order->id_shop);

        if (
            !isset($rule->id) ||
            !isset($rule->code) ||
            (string) $rule->code === ''
        ) {
            throw new Exception('Failed to rule cart rule with id ' . $ruleId);
        }

        return (string) $rule->code;
    }

    /**
     * Retrieve shipping fee line.
     *
     * @param PrestaOrder $order
     * @return array
     * @throws Exception
     */
    public function getShippingLine(PrestaOrder $order)
    {
        // Retrieve raw carrier data from order.
        $carrierData = $this->getCarrierData($order);

        // Validate carrier data.
        $this->validateCarrierData($carrierData);

        // Calculate shipping tax percent.
        $shippingTax = $this->calculateShippingTax(
            $carrierData['shipping_cost_tax_incl'],
            $carrierData['shipping_cost_tax_excl']
        );

        return parent::compileShippingLine(
            $carrierData['shipping_cost_tax_excl'],
            $shippingTax,
            $this->getShippingLineDescription($order)
        );
    }

    /**
     * Retrieve raw carrier data from order.
     *
     * @param PrestaOrder $order
     * @return array
     * @throws Exception
     */
    public function getCarrierData(PrestaOrder $order)
    {
        $carriers = $order->getShipping();

        if (!is_array($carriers)) {
            throw new Exception('Expected carriers to be formatted as array.');
        }

        if (count($carriers) > 1) {
            throw new Exception('Expected single carrier.');
        }

        return (array) $carriers[0];
    }

    /**
     * Validate carrier data.
     *
     * @param array $carrier
     * @return $this
     * @throws Exception
     */
    public function validateCarrierData(array $carrier)
    {
        if (
            !isset($carrier['id_carrier']) ||
            !isset($carrier['shipping_cost_tax_incl']) ||
            !isset($carrier['shipping_cost_tax_excl'])
        ) {
            throw new Exception('Missing carrier data.');
        }

        return $this;
    }

    /**
     * Retrieve shipping line description (shipping carrier name/identifier). We also sanitize the name.
     *
     * @param PrestaOrder $order
     * @return string
     * @throws Exception
     */
    public function getShippingLineDescription(PrestaOrder $order)
    {
        $carrier = new Carrier($order->id_carrier, $order->id_lang);

        if (!isset($carrier->name) || !isset($carrier->id_reference)) {
            throw new Exception('Invalid shipping carrier. Missing name/reference.');
        }

        return (string) preg_replace('/[^a-z0-9\_]/isU', '', (
            preg_replace('/\ /', '_', $carrier->name) .
            '_' .
            $carrier->id_reference
        ));
    }
}
