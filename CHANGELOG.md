# Version 2.7.11

* [PRESTA17-123](https://resursbankplugins.atlassian.net/browse/PRESTA17-123) Customer data sanitizing
* [PRESTA17-122](https://resursbankplugins.atlassian.net/browse/PRESTA17-122) Timeout-hantering
* [PRESTA17-121](https://resursbankplugins.atlassian.net/browse/PRESTA17-121) Logging Address::Billing Synchronization
* [PRESTA17-118](https://resursbankplugins.atlassian.net/browse/PRESTA17-118) Render timeout warnings \(or any exception\) on callback problems
* [PRESTA17-117](https://resursbankplugins.atlassian.net/browse/PRESTA17-117) Throw internal exceptions in place\(\) with proper error codes

# Version 2.7.10

* [PRESTA17-120](https://resursbankplugins.atlassian.net/browse/PRESTA17-120) 2.7.10 marker.
* [PRESTA17-119](https://resursbankplugins.atlassian.net/browse/PRESTA17-119) ssn are validated as required on session requests

# Version 2.7.9

* [PRESTA17-115](https://resursbankplugins.atlassian.net/browse/PRESTA17-115) When resolveShippingAddress fails

# Version 2.7.8

* [PRESTA17-113](https://resursbankplugins.atlassian.net/browse/PRESTA17-113) Cronjob is unable to complete orderprocessing due to internal errors
* [PRESTA17-110](https://resursbankplugins.atlassian.net/browse/PRESTA17-110) Billingaddress inloggat läge
* [PRESTA17-107](https://resursbankplugins.atlassian.net/browse/PRESTA17-107) SSN-less direct payments register payments but not fulfilling them.
* [PRESTA17-104](https://resursbankplugins.atlassian.net/browse/PRESTA17-104) Customized product delivery address id determination error
* [PRESTA17-101](https://resursbankplugins.atlassian.net/browse/PRESTA17-101) Address data is not properly synchronized when data is changed by customer where the rco frontend can not reach the address
* [PRESTA17-114](https://resursbankplugins.atlassian.net/browse/PRESTA17-114) Nytt sätt att hantera fraktadresser på
* [PRESTA17-112](https://resursbankplugins.atlassian.net/browse/PRESTA17-112) Om fraktadressen i $cart skiljer sig från den $deliveryAddress som är sätts i prepareForOrderPlacement \(och saknar värde\), ändra cartens delivery till billing
* [PRESTA17-111](https://resursbankplugins.atlassian.net/browse/PRESTA17-111) Implement purchasedenied
* [PRESTA17-109](https://resursbankplugins.atlassian.net/browse/PRESTA17-109) Delivery address på inloggad kund ger fel fraktadress i slutänden
* [PRESTA17-108](https://resursbankplugins.atlassian.net/browse/PRESTA17-108) Prefilled userdata
* [PRESTA17-106](https://resursbankplugins.atlassian.net/browse/PRESTA17-106) Delivery address matching with phone number involved
* [PRESTA17-103](https://resursbankplugins.atlassian.net/browse/PRESTA17-103) Synchronize address data with getPayment \(again\) 

# Version 2.7.7

* [PRESTA17-91](https://resursbankplugins.atlassian.net/browse/PRESTA17-91) Totals not rounded

# Version 2.7.6 (2.7.3-2.7.6)

* [PRESTA17-87] First investigation of compatiblity for 1.7.7
* [PRESTA17-89] Footer breaks child templates with missing footers at checkout page.  
* [PRESTA17-88] Controller breaks running for 1.7.7 in some instances (cleaner.php).
* Releases includes upgrade of ecomphp.

# Version 2.7.2

* [PRESTA17-75] - ecom1.3-PHP8-compliance at ecomphp-level
* [PRESTA17-76] - Cancellationscript should accept cancellations on "payment not found" in cleanupscript (PRESTA16-233)
* [PRESTA17-77] - validateCredentials (PRESTA16-124)
* [PRESTA17-78] - Check rebuildcart (PRESTA16-235)
* [PRESTA17-79] - Logo transparency (PRESTA16-56)
* [PRESTA17-80] - Split statuses for credited and annulled
* [PRESTA17-81] - Protect cancel-cron from 0-order-cancellation
* [PRESTA17-66] - Redan vid "lägg i varukorg" kan pluginet få för sig att den ska uppdatera resurs sida av varukorgen
* [PRESTA17-70] - Deprecated displayPrice since 1.7.6.0

# Version 2.7.1

* [PRESTA17-72] - Session warnings in checkout is probably not session warnings

# Version 2.7.0

* From v2.6.4, 2.7.0 is created and customized for PrestaShop v1.7

* [PRESTA16-204] - [1.7] Sessioner
* [PRESTA16-225] - Compare and (try to) import 1.7 hotfixes.
* [PRESTA16-228] - [Kassa] Specialköp: Customized products (om de finns) skall INTE misslyckas till skillnad från Presta 1.7 - yet they still do. And they don't exist in 1.6 the same way.
* [PRESTA17-27] - När varukorgen töms (dvs. när man tar bort samtliga varor) så skall man pekas om
* [PRESTA17-30] - Uppgradera till nyare ecom
* [PRESTA17-33] - När varukorgen töms bör vi unset:a sessionsdata (destroy iframe / id)
* [PRESTA17-36] - Update user-agent data to contain PHP-version
* [PRESTA17-37] - Ta bort session timeout 
* [PRESTA17-38] - Kolla om session aktiv vid genomför köp
* [PRESTA17-41] - Verifiera att statushanteringen fungerar
* [PRESTA17-58] - PrestaShop module will conflict with current version for 1.6
* [PRESTA17-63] - Move checkcart (from PRESTA17-27) into separate call controller
* [PRESTA17-71] - Log controller redirect hooks
* [PRESTA17-2] - Som ADMIN vill jag kunna göra inställningar i general settings
* [PRESTA17-3] - Som ADMIN vill jag kunna hämta betalmetoder att göra inställningar för att visa "Delbetala från..." på produktsidan
* [PRESTA17-4] - Som ADMIN vill jag kunna registrera callbacks
* [PRESTA17-5] - Som kund vill jag kunna hantera min varukorg i kassan
* [PRESTA17-6] - Som kund vill jag kunna använda min rabattkod i kassan
* [PRESTA17-7] - Som kund vill jag kunna checka ut med RCO i kassan
* [PRESTA17-8] - Som kund vill jag veta om köpet gått igenom eller inte
* [PRESTA17-17] - "Vanlig" installation med nuvarande repobaserade release  funkar inte ordentligt
* [PRESTA17-35] - Can't reset plugin on tests
* [PRESTA17-43] - [Session] Sessioner verkar inte starta.
* [PRESTA17-44] - Save-knappen i admin gör att listan betalmetoder försvinner och inställning för annuity nollställs
* [PRESTA17-45] - Ibland skapas inte order i Prestashop
* [PRESTA17-46] - Fraktval uppdaterar inte totalrutan till höger
* [PRESTA17-47] - I 1.6 visades betalmetoden i orderlistan
* [PRESTA17-52] - RESURSBANK is (still) not defined
* [PRESTA17-54] - Errormessage vid visa order
* [PRESTA17-55] - Köp genomfördes utan att skapas i prestashop
* [PRESTA17-60] - Lowercase paymentmethod ids not allowed for newer merchant portal
* [PRESTA17-61] - Konstig statushantering
* [PRESTA17-62] - Failurl fails due to changes in ecommerce (Resurs) - Urls should be encoded instead.
* [PRESTA17-64] - Weird behaviour in checkout
* [PRESTA17-67] - Fetching callbacks by rest in initial admin-view may shutdown the configuration entirely if exceptions occur
* [PRESTA17-68] - (Not a bug) Problems with /För in cart-to-checkout-url (Documentation only)
* [PRESTA17-69] - Clicking from cart to checkout not always redirecting to proper RCO-page
* [PRESTA17-11] - Admin användare kan sätta miljö.
* [PRESTA17-12] - Payment method list and configuration
* [PRESTA17-13] - Admin användare kan spara settings i databasen
* [PRESTA17-14] - En admin användare kan fylla i sina resurs bank credentials
* [PRESTA17-15] - Validering för produktion
* [PRESTA17-16] - Validering för test
* [PRESTA17-18] - Description is undefined index
* [PRESTA17-20] - Make payment methods easier to read
* [PRESTA17-21] - Annuity (and methods?) are not cleaned up on uninstall/reset plugin
* [PRESTA17-22] - [Bug] First time activating (and/or reset plugin) issues
* [PRESTA17-23] - Move annuity config data from config.php
* [PRESTA17-24] - Gå igenom dem olika typerna av uträkningar (enligt acceptanskraven i PRESTA17-6) och kontrollera så samtliga fungerar.
* [PRESTA17-26] - Fixa så rabatt följer med i iframe:n.
* [PRESTA17-31] - Upgrade configuration to work with new non abstract class
* [PRESTA17-32] - Bring in translations from annuity parts
* [PRESTA17-34] - Som utloggad måste man kunna byta fraktmetod
* [PRESTA17-39] - Admin användare kan registrera callbacks
* [PRESTA17-40] - Visa ett success-meddelande när callbacks registreras
* [PRESTA17-48] - Bug: Failpage not showing, directed to orderopc.
* [PRESTA17-49] - Bug: Success-sidan visas inte längre
* [PRESTA17-51] - Annuity factor list charset issue
* [PRESTA17-53] - Registrera-knappen funkar inte utan att save-knappen är använd
* [PRESTA17-56] - [Bug] Failurl works but built in methods does not cancel the prior order


# Version 2.6.5 - 2.6.9

## Imported notes

* Also included in version releases that is missing, is the ecomphp-library with PHP 8.0-compliances.

## The rest of the release notes

* [PRESTA16-222] - Update presta 1.6 ecomphp to latest (And check the bitwise controls for order statuses)
* [PRESTA16-208] - [PartPayment] Partpayment Implementation
* [PRESTA16-212] - [PartPayment] Layout cleanup for admin
* [PRESTA16-89] - Make sure that the failure URL is loaded if order placement fails after clicking the place order button
* [PRESTA16-138] - Translate everything.
* [PRESTA16-145] - Callbacks no longer work after re-registering them
* [PRESTA16-152] - If there are no registered callbacks the button to register callbacks will fail to display.
* [PRESTA16-182] - Uppgradera till senaste versionen av ECom
* [PRESTA16-192] - Kontrollera användarnamn/lösenord och visa ett felmeddelande i konfigen om dessa inte stämmer
* [PRESTA16-196] - Felsök ärende åt Staffanstorp. När kund går till gateway och sedan avbryter skall dem komma tillbaka till kassan och varukorgen skall återskapas.
* [PRESTA16-204] - [1.7] Sessioner
* [PRESTA16-205] - [PartPayment] Admin-panel configuration (Backend)
* [PRESTA16-206] - [PartPayment] Customer view (Frontend)
* [PRESTA16-209] - [Automatisk annullering] Undvik beställningar där callbacks inkommit
* [PRESTA16-210] - [PartPayment] Part payment in checkout
* [PRESTA16-211] - [PartPayment] getCostOfPurchaseHtml popup.
* [PRESTA16-220] - Översättningar till annuity-delarna
* [PRESTA16-221] - [ User Agent ] Lägg till version av Prestashop för att underlätta felsökning
* [PRESTA16-63] - Ingen orderbekräftelselandningssida efter köp
* [PRESTA16-67] - RCO laddas inte om man avbyter Nets och kommer tillbaka till kassan
* [PRESTA16-103] - Callback funkar inte vid återställa plugin
* [PRESTA16-216] - When activating part payment method first time, period is not saved
* [PRESTA16-217] - Fel belopp skickas till "Read more"
* [PRESTA16-218] - Listan över betalmetoder refreshas inte vid kontobyta test till prod
* [PRESTA16-219] - RESURSBANK is not defined (js)
* [PRESTA16-223] - Things that can break on upgrades
* [PRESTA16-224] - Annuity (and methods) are not cleaned up during uninstall/reset
* [PRESTA16-229] - "Pay from" is active even if it is not properly configured
* [PRESTA16-225] - Compare and (try to) import 1.7 hotfixes.
* [PRESTA16-226] - [Kassa] Testköp som gäst och inloggad skall fortsatt fungera utan exceptions
* [PRESTA16-227] - [Admin] Statusar för varje order skall fortsatt fungera utan exceptions
* [PRESTA16-228] - [Kassa] Specialköp: Customized products (om de finns) skall INTE misslyckas till skillnad från Presta 1.7
* [PRESTA16-213] - [PartPayment] Display payment methods
* [PRESTA16-214] - [PartPayment] Display annuity options in admin differently
* [PRESTA16-215] - thead for payment methods list



# Version 1.0.0

* [PRESTA16-51] - Ställ in Prestashop vid installation utav modul
* [PRESTA16-55] - Ta bort grupperingsinställning i admin under modul
* [PRESTA16-57] - Sätt valt betalsätt på ordern
* [PRESTA16-58] - Set v1.0.0 as release version
* [PRESTA16-59] - CSS for admin lost


# Version 0.9.3

* [PRESTA16-17] - Translations
* [PRESTA16-31] - Set icon to Resurs status
* [PRESTA16-52] - Optimize PRESTA-50 (callbacks) furthermore
* [PRESTA16-32] - Follow URLs in higher security environments (curl setting $followLocationEnforce)
* [PRESTA16-35] - Two "save"-clicks are required for callbacks to be saved on credential changes
* [PRESTA16-38] - Need to save two times when changing environment in admin?
* [PRESTA16-50] - Configuration saves slow - Optimize speed by limiting callback update requests to a dynamic interval (subtask/fix for PRESTA-35)
* [PRESTA16-53] - Test event triggers goes to wrong environment (Hardcoded)


# Version 0.9.2

* [PRESTA16-36] - Remove ecomphp tests in final version
* [PRESTA16-37] - Show registered callbacks view (admin gui)
* [PRESTA16-40] - Replace callback if-conditions with case
* [PRESTA16-41] - Callback URL validation
* [PRESTA16-45] - Cache payment methods on checkout page for invoice fee-handling
* [PRESTA16-47] - No calllback testing when "TEST" does not exist
* [PRESTA16-48] - Visa betalmetod i meddelandet
* [PRESTA16-49] - Blå ruta på order i admin
* [PRESTA16-9] - Support: Discount
* [PRESTA16-13] - Handle callbacks by cartid (until orderids are fixed) - Primary functions
* [PRESTA16-33] - Created default statuses with generated mail-responses to customers
* [PRESTA16-34] - Callback adjustments (for PRESTA-13)
* [PRESTA16-44] - Säkerställ att moms och priser skickas korrekt
* [PRESTA16-46] - [ecommerce] getCallbacksByRest() does not entirely reflect the truth in ecommerce

# Version 0.9.1

* [PRESTA16-18] - Country-selector with guess checkout
* [PRESTA16-24] - Show in gui which version of the plugin the current agent is running.
* [PRESTA16-28] - Add prestashop Order# as metadata at ResursBank
* [PRESTA16-29] - Build Resurs-based order statuses
* [PRESTA16-21] - Live / Prod inställningar
* [PRESTA16-30] - Address update fails

# Version 0.9.0

* [PRESTA16-1] - ResursBank logo i lista av moduler
* [PRESTA16-2] - I konfigurera Resurs Bank Ceckout
* [PRESTA16-3] - Bort med steg 1 i kassan
* [PRESTA16-14] - Register callbacks ECom1.2-compatibility
* [PRESTA16-5] - Ändra Artikelbeskrivning
* [PRESTA16-6] - Dold checkout
* [PRESTA16-7] - Synka adressatinformation
* [PRESTA16-8] - Tillbaka till varukorgen vid "fel"
* [PRESTA16-11] - Rename plugin to lowercase
* [PRESTA16-15] - Duplicate terms checkbox

